export const MAIN_STAT = 'main';
export const SIT_STAT = 'situational';
export const VIO_STAT = 'violation';

export const MEASURE_PERCENT = '%';
export const MEASURE_SPEED = 'км/ч';
export const MEASURE_UNITS = 'ед';
export const MEASURE_DISTANCE = 'км';

export const Lang = {
  kk: 'Qazakh tili',
  kz: 'Қазақ тілі',
  ru: 'Русский язык'
};

export const STUDENT_TYPE = 3;

export const Male = [{ value: 'муж', name: 'муж' }, { value: 'жен', name: 'жен' }];
