const paths = {
  homePage: '/',
  loginPage: '/login',
  statementPage: '/statement',
  testingPage: '/testing',
  contacts: '/contacts',
  review: '/review',
  agreementPage: '/agreement',
  journalPage: '/journal',
  createJournal: '/createJournal',
  parentsPage: '/parents',
  apprenticePage: '/apprentice',
  groupsPage: '/groups',
  schedulesPage: '/schedules',
  roles: '/roles',
  message: '/message',
  users: '/users',
  filingApplication: '/filingApplication',
  homeWork: '/homeWork',
  service: '/service',
  achievements: '/achievements',
  passTest: '/passTest',
  resultTest: '/resultTest:id'
};

export default paths;
