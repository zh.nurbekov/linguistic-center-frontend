import axios from 'axios';
import paths from '../_helpers/paths';
import { history } from './history';
import { NotificationManager } from 'react-notifications';
import { localSecureStorage } from '../_app/App';

export const Api = axios.create({
  baseURL: '/',
  headers: { 'Content-Type': 'application/json' }
});

const configureAuth = config => {
  const user = localSecureStorage.getItem('user');
  if (!config.headers.Authorization && user) {
    const newConfig = {
      headers: {},
      ...config,
      auth: {
        username: user.username,
        password: user.password
      }
    };
    return newConfig;
  }
  return config;
};

const unauthorizedResponse = async error => {
  if (error.response && error.response.status === 401) {

    if (window.location.pathname !== paths.loginPage) {
      localSecureStorage.removeItem('token');
      localSecureStorage.removeItem('user');
      localSecureStorage.removeItem('refreshToken');
      localSecureStorage.removeItem('expiredAt');
    }
  }
  return Promise.reject(error);
};

Api.interceptors.request.use(configureAuth, e => Promise.reject(e));
Api.interceptors.response.use(r => r, unauthorizedResponse);
