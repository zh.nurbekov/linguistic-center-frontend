import sortReducers from '../utils/sortReducers';
import dialogReducer,{ dialogModule } from '../components/DialogModal/DialogModalDucks';
import progressReducer,{ progressModule } from '../components/ProgressBar/ProgressBarDucks';
import themeReducer,{ themeModule } from '../components/ThemeProvider/ThemeDucks';
import loginReducer,{loginModule} from "../pages/LoginPage/LoginDucks";
import usersReducer,{usersModule} from "../pages/UserManagePage/Users/UsersDucks";
import rolesReducer,{rolesModule} from "../pages/UserManagePage/Roles/RolesDucks";
import testingReducer,{testingModule} from "../pages/TestingPage/TastingDucks";
import PassTestReducer,{PassTestModule} from "../pages/PassTest/PassTestDucks";
import schedulesReducer,{ schedulesModule } from '../pages/Schedules/SchedulesDucks';
import groupReducer,{ groupModule } from '../pages/Groups/GroupDucks';
import journalReducer,{ journalModule } from '../pages/Journal/JournalDucks';
import messageReducer,{ messageModule } from '../pages/Message/MessageDucks';
import homeWorkReducer,{ HomeWorkModule } from '../pages/HomeWork/HomeWorkDucks';

export const rootReducer = sortReducers({
  [dialogModule]: dialogReducer,
  [loginModule]: loginReducer,
  [progressModule]: progressReducer,
  [themeModule]: themeReducer,
  [usersModule]: usersReducer,
  [rolesModule]: rolesReducer,
  [testingModule]: testingReducer,
  [PassTestModule]: PassTestReducer,
  [schedulesModule]: schedulesReducer,
  [groupModule]: groupReducer,
  [journalModule]: journalReducer,
  [messageModule]: messageReducer,
  [HomeWorkModule]: homeWorkReducer,

});
