import React from 'react';
import paths from './paths';
import Page404 from '../pages/Page404/Page404';
import Page403 from '../pages/Page403/Page403';
import MainIcon from '@material-ui/icons/Lens';
import HomePage from '../pages/HomePage/HomePage';
import UserManage from '../pages/UserManagePage/UserManage';
import Testing from '../pages/TestingPage/Testing';
import PassTest from '../pages/PassTest/PassTest';
import Login from '../pages/LoginPage/Login';
import FilingApplication from '../pages/FilingApplications/FilingApplication';
import Schedules from '../pages/Schedules/Schedules';
import Groups from '../pages/Groups/Groups';
import Journal from '../pages/Journal/Journal';
import CreateJournal from '../pages/Journal/CreateJournal';
import Message from '../pages/Message/Message';
import HomeWork from '../pages/HomeWork/HomeWork';
import Service from '../pages/Service/Service';
import Achievements from '../pages/Achievements/Achievements';
import Contacts from '../pages/Contacts';
import Review from '../pages/Review/Review';
import Statement from '../pages/Statement/Statement';
import { PERMISSIONS, PERMISSIONS_TREE } from './permissions';
import Contracts from '../pages/Contracts/Contract';

const routers = [
  {
    path: paths.loginPage,
    title: 'Авторизация',
    component: Login,
    withoutAuth: true,
    withoutHeader: true,
    exact: true
  },
  {
    path: paths.homePage,
    title: 'Авторизация',
    component: HomePage,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.service,
    title: 'Авторизация',
    component: Service,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.contacts,
    title: 'Авторизация',
    component: Contacts,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.review,
    title: 'Авторизация',
    component: Review,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.achievements,
    title: 'Авторизация',
    component: Achievements,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.statementPage,
    title: 'Заявки',
    component: Statement,
    permissions: PERMISSIONS.STATEMENT,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.testingPage,
    title: 'Тестирование',
    component: Testing,
    permissions: PERMISSIONS.TESTING,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },

  {
    path: paths.review,
    title: 'Отзывы',
    component: Review,
    permissions: PERMISSIONS.REVIEW,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.journalPage,
    title: 'Журнал',
    component: Journal,
    permissions: PERMISSIONS.JOURNAL,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.createJournal,
    title: 'Журнал',
    component: CreateJournal,
    permissions: 'main',
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.groupsPage,
    title: 'Группы',
    component: Groups,
    permissions: PERMISSIONS.GROUPS,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.schedulesPage,
    title: 'Расписания',
    component: Schedules,
    permissions: PERMISSIONS.SCHEDULER,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.message,
    title: 'Сообщения',
    component: Message,
    sideBarItem: true,
    permissions: PERMISSIONS.MESSAGE,
    // icon: <Icon type="message" />,
    exact: true
  },
  {
    path: paths.passTest,
    title: 'Пройти тестирование',
    component: PassTest,
    permissions: PERMISSIONS.PASS_TEST,
    sideBarItem: true,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.users,
    title: 'Управление пользователями',
    component: UserManage,
    permissions: PERMISSIONS.USERS,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    sideBarItem: true,
    exact: true
  },
  {
    path: paths.roles,
    title: 'Роли',
    component: UserManage,
    permissions: PERMISSIONS.ROLES,
    icon: <MainIcon color={'secondary'} />,
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.filingApplication,
    component: FilingApplication,
    permissions: 'main',
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.homeWork,
    component: HomeWork,
    sideBarItem: true,
    permissions: PERMISSIONS.HOME_WORK,
    title: 'Домашнее задание',
    withoutAuth: true,
    exact: true
  },
  {
    path: paths.resultTest,
    component: FilingApplication,
    permissions: 'main',
    withoutAuth: true,
    exact: true
  },
  {
    path: '/404',
    title: 'error',
    component: Page404,
    withoutAuth: true,
    exact: true
  },
  {
    path: '/403',
    title: 'error',
    component: Page403,
    withoutAuth: true,
    exact: true
  }
];

export default routers;
