//TODO доделать константы чтобы проава везде тягулись отсюда

export const PERMISSIONS = {
  STATEMENT: 'statement',
  TESTING: 'testing',
  JOURNAL: 'journal',
  JOURNAL_CREATE_AND_EDIT: 'journalCreateAndEdit',
  //CREATE_JOURNAL: 'createJournal',
  GROUPS: 'groups',
  GROUPS_EDIT: 'groupsEdit',
  SCHEDULER: 'schedules',
  SCHEDULER_CREATE_AND_EDIT: 'schedulesCreateAndEdit',
  ROLES: 'roles',
  MESSAGE: 'message',
  USERS: 'users',
  HOME_WORK: 'homeWork',
  HOME_WORK_CREATE: 'homeWorkCreate',
  PASS_TEST: 'passTest',
  REVIEW: 'review'
};

export const PERMISSION_NAMES = {
  /*Раздел Базовые станции*/
  [PERMISSIONS.STATEMENT]: 'Доступ к разделу "Заявки"',
  [PERMISSIONS.TESTING]: 'Доступ к разделу "Тестирование"',
  [PERMISSIONS.JOURNAL]: 'Доступ к разделу "Журнал"',
  [PERMISSIONS.JOURNAL_CREATE_AND_EDIT]:
    'Доступ к созданию и редактированию записей в  разделе "Журнал" ',
  [PERMISSIONS.GROUPS]: 'Доступ к разделу "Группы"',
  [PERMISSIONS.GROUPS_EDIT]: 'Доступ к созданию и редактированию записей в  разделе "Группы" ',
  [PERMISSIONS.SCHEDULER]: 'Доступ к разделу "Расписания"',
  [PERMISSIONS.SCHEDULER_CREATE_AND_EDIT]:
    'Доступ к  создание записей в разделе "Расписания"',
  [PERMISSIONS.ROLES]: 'Доступ к разделу "Роли"',
  [PERMISSIONS.MESSAGE]: 'Доступ к разделу "Сообщения"',
  [PERMISSIONS.USERS]: 'Доступ к разделу "Пользователи"',
  [PERMISSIONS.PASS_TEST]: 'Доступ к разделу "Пройти тестирование"',
  [PERMISSIONS.HOME_WORK]: 'Доступ к разделу "Домашнее задание"',
  [PERMISSIONS.HOME_WORK_CREATE]:
    'Доступ к созданию записей в разделе "Домашнее задание"',
  [PERMISSIONS.REVIEW]: 'Доступ к разделу "Отзывы"'
};

export const PERMISSIONS_TREE = {
  [PERMISSIONS.STATEMENT]: [],
  [PERMISSIONS.TESTING]: [],
  [PERMISSIONS.GROUPS]: [PERMISSIONS.ROLES, PERMISSIONS.USERS],
  [PERMISSIONS.SCHEDULER]: [],
  [PERMISSIONS.MESSAGE]: [],
  [PERMISSIONS.PASS_TEST]: []
};
