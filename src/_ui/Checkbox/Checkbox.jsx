import React from "react"
import {FormControlLabel} from "@material-ui/core";
import Field from "../Form/Field";
import CheckBox from '@material-ui/core/Checkbox';
import {makeStyles} from "@material-ui/core/styles";

const styles = makeStyles(() => ({
  labelFormControl: {
    marginLeft: 35,
    color: props => props.color || '#263238'
  }
}))

const Checkbox = ({label, color, ...props}) => {
  const checkBox = <CheckBox color={"primary"} {...props}  />
  const classes = styles({color});

  if (label) {
    return (
      <FormControlLabel
        classes={{
          label: classes.labelFormControl
        }}
        control={checkBox}
        label={label}
      />
    )
  }

  return checkBox
}

const CheckboxField = ({withoutForm, name, ...props}) => {
  if (!withoutForm) {
    return (
      <Field name={name}>
        {({value = false, onChange, ...fieldProps}) => (
          <Checkbox
            checked={value}
            onChange={(e) => onChange(e.target.checked)}
            {...fieldProps}
            {...props}
          />
        )}
      </Field>
    )
  }

  return <Checkbox {...props} name={name}/>
}

export default CheckboxField