import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import MuiSelect from '@material-ui/core/Select';
import Field from '../Form/Field';
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles } from '@material-ui/core/styles';

const getChecked = (optionValue, value) =>
  value instanceof Array ? value.includes(optionValue) : value === optionValue;

const getOptionsValue = (options, value) => {
  let result = [];
  options.forEach(option => {
    getChecked(option.value, value) && result.push(option['name']);
  });
  return result.join(', ');
};

const useStyles = makeStyles(() => ({
  selectLabel: {
    '& .MuiInputLabel-shrink': {
      transform: 'translate(0, -5.5px) scale(0.75)!important'
    },
    '& .MuiInputLabel-formControl': {
      left: '14px',
      transform: 'translate(0, 11px) scale(1)'
    }
  },
  helperTextRoot: {
    marginLeft: 14,
    marginTop: 4
  }
}));

function SelectComponent(props) {
  const {
    name,
    options = [],
    value,
    onChange,
    multiple,
    disabled,
    label,
    variant
  } = props;
  const styles = useStyles();
  return (
    <FormControl fullWidth disabled={disabled} className={styles.selectLabel}>
      <InputLabel id="select">{label}</InputLabel>
      <MuiSelect
        placeholder={props.placeholder}
        labelId="select"
        name={name}
        multiple={multiple}
        value={multiple ? value || [] : value}
        onChange={event => () => {
          onChange(event);
        }}
        variant={'standard'}
        displayEmpty
        error={!!props.error}
        MenuProps={{ style: { maxWidth: 650 } }}
        {...props}
      >
        <MenuItem value={undefined} selected={true} disabled>
          <ListItemText primary={props.placeholder} />
        </MenuItem>
        {options.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {multiple && (
              <Checkbox
                color="primary"
                checked={getChecked(option.value, value)}
                style={{ marginRight: 5, width: 30 }}
              />
            )}
            <ListItemText primary={option['name']} />
          </MenuItem>
        ))}
      </MuiSelect>
      <FormHelperText classes={{ root: styles.helperTextRoot }} error={!!props.error}>
        {props.helperText}
      </FormHelperText>
    </FormControl>
  );
}

const Select = props => {
  const { withoutForm, name, onChange, ...restProps } = props;
  return withoutForm ? (
    <SelectComponent name={name} onChange={onChange} {...restProps} />
  ) : (
    <Field name={name}>
      {({ onChange, value = '', ...fieldProps }) => (
        <SelectComponent
          value={value}
          name={name}
          onChange={event =>
            (() => {
              onChange(event.target.value);
            })()
          }
          {...restProps}
          {...fieldProps}
        />
      )}
    </Field>
  );
};

Select.propsTypes = {
  withoutForm: PropTypes.bool,
  name: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  variant: PropTypes.string
};

Select.defaultProps = {
  withoutForm: false
};

export default Select;
