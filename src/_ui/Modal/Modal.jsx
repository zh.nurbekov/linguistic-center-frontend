import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core';

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

const DialogTitle = withStyles(styles)(props => {
  const { palette } = useTheme();
  const { children, classes, onClose, titleDesc, ...other } = props;
  return (
    <MuiDialogTitle disableTypography {...other}>
      <div className="flex items-center p2">
        <div className="mr2">
          <Typography
            style={{ fontWeight: 600, fontSize: 18, color: palette.text.primary }}
          >
            {children}
          </Typography>
        </div>
        {titleDesc && (
          <div
            style={{ padding: '2px 20px', borderRadius: 4, border: '1px solid black' }}
          >
            <Typography>{titleDesc}</Typography>
          </div>
        )}
      </div>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

function Modal({ title, children, titleDesc, ...rest }) {
  return (
    <Dialog fullWidth open {...rest}>
      <DialogTitle onClose={rest.onClose} titleDesc={titleDesc}>
        {title}
      </DialogTitle>
      {children}
    </Dialog>
  );
}

export default Modal;
