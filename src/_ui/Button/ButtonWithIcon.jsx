import Button from '@material-ui/core/Button';
import React from 'react';
import { makeStyles, Tooltip } from '@material-ui/core';

export const useStyles = makeStyles(theme => ({
  exportBtn: {
    padding: 10,
    minWidth: '35px !important'
  }
}));

const ButtonWithIcon = ({ icon, title, ...props }) => {
  const classes = useStyles();
  return (
    <Tooltip title={title || ''}>
      <Button
        variant={'outlined'}
        className={classes.exportBtn}
        children={icon}
        {...props}
      />
    </Tooltip>
  );
};

export default ButtonWithIcon;
