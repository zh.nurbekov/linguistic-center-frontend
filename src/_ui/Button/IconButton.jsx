import React from 'react';
import MuiIconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

//удалил свойство loading, оно было передано просто так, даже если в api button,
// такого свойство нет, из-за этого в консоли показывало ошибку.
function IconButton({ icon, tooltip, ...rest }) {
  if (tooltip) {
    return (
      <Tooltip title={tooltip} placement="top" className="right">
        <MuiIconButton {...rest} children={icon} disabled={rest.disabled} />
      </Tooltip>
    );
  }

  return <MuiIconButton {...rest} children={icon} />;
}

export default IconButton;
