import React from "react"
import { Button, Tooltip } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const styles = makeStyles(theme => ({
  buttonIconRoot: {
    padding: "8px !important",
    minWidth: 0,
    borderColor: props => props.borderColor ? props.borderColor : 'rgba(0, 0, 0, 0.3)'
  },
  buttonIconSvg: {
    width: 12,
    height: 12
  }
}))

const Btn = ({icon, borderColor, ...props}) => {
  const classes = styles({borderColor})
  return (
    <Button
      classes={{
        root: classes.buttonIconRoot
      }}
      variant="outlined"
      {...props}
    >
      {React.cloneElement(icon, {className: classes.buttonIconSvg})}
    </Button>
  )
}

const ButtonIts = ({tooltip, icon, ...props}) => {
  if (tooltip) {
    return (
      <Tooltip title={tooltip} placement="bottom" className="right">
        <Btn icon={icon} {...props}/>
      </Tooltip>
    )
  }

  return <Btn icon={icon} {...props}/>
}

export default ButtonIts