import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Field from '../Form/Field';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import IconButton from '../Button/IconButton';

const Input = props => {
  const {
    withoutForm,
    name,
    onChange,
    maxLength,
    shrink = false,
    showPassViewButton,
    showWarningIconOnError,
    variant = 'standard',
    ...restProps
  } = props;
  const [showPass, setShowPass] = React.useState(false);

  const additionalInputProps = {
    ...restProps.InputProps,
    endAdornment: (
      <div className={'flex items-end justify-end'}>
        {showPassViewButton && (
          <IconButton
            tooltip={showPass ? 'Скрыть' : 'Показать'}
            style={{ padding: 2 }}
            onClick={() => setShowPass(!showPass)}
            icon={showPass ? <VisibilityOff /> : <Visibility />}
          />
        )}
      </div>
    )
  };

  const shrinkProps = shrink ? { shrink: shrink } : {};
  const type =
    showPassViewButton && !showPass
      ? 'password'
      : restProps.type === 'password'
      ? 'text'
      : restProps.type;

  return withoutForm ? (
    <TextField
      name={name}
      onChange={onChange}
      InputProps={additionalInputProps}
      type={type}
      {...restProps}
      variant={variant}
      inputProps={{ maxLength: maxLength }}
    />
  ) : (
    <Field name={name} helperText={restProps.helperText} {...restProps}>
      {({ onChange, name, value = '', ...fieldProps }) => {
        const props = {
          name,
          type,
          InputLabelProps: { ...shrinkProps },
          inputProps: { maxLength: maxLength },
          onChange: event => {
            if (type === 'file') {
              onChange(event.currentTarget.files[0]);
            } else {
              onChange(event.target.value);
            }
          },
          value,
          ...restProps,
          ...fieldProps
        };

        if (type === 'file') {
          delete props.value;
        }

        return (
          <TextField
            {...props}
            InputProps={additionalInputProps}
            type={type}
            variant={variant}
          />
        );
      }}
    </Field>
  );
};

Input.propsTypes = {
  withoutForm: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired
};

Input.defaultProps = {
  withoutForm: false,
  showWarningIconOnError: false,
  label: ''
};

export default Input;
