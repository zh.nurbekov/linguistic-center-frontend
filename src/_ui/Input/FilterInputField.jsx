import { useEffect, useState } from 'react';
import React from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import {Grid} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import {User} from "../../../types/Login";

const CLEAR_ICON_WIDTH = 30;

export const useStyles = makeStyles(theme => ({
  input: props => ({
    fontSize: props.fontSize,
  })
}));


function FilterInputField({
  type = "text",
  placeholder = "",
  value = '',
  onChange = (val) => {},
  needCheckLength = false,
  variant,
  label = "",
  name = "",
  size = 'small',
  fontSize = 14
}) {
  const [timeOutId, registerTimeOut] = useState(null);
  const [fieldValue, setFieldValue] = useState(value || '');
  const textType = type === 'text';
  const classes = useStyles({ fontSize });


  const clear = () => {
    onChange(null);
    setFieldValue('');
  };

  const handleChange = newValue => {
    if (!needCheckLength && (parseInt(newValue) || textType)) {
      onChange(newValue);
      setFieldValue(newValue);
      return;
    }

    if (textType && newValue.length <= 2) {
      if (value) {
        clearTimeout(timeOutId);
        newValue.length <= 2 && onChange(null)
      }
      setFieldValue(newValue);
    } else {
      if (parseInt(newValue) || type === 'text') {
        clearTimeout(timeOutId);
        registerTimeOut(
          setTimeout(() => {
            onChange(newValue);
          }, 1000)
        );
        setFieldValue(newValue);
      }
    }
  };

  const inputProps = {
    endAdornment: fieldValue.length > 0 ? (
        <div style={{width: CLEAR_ICON_WIDTH}}>
        <InputAdornment position="end" >
          <IconButton onClick={clear} size="small">
            <CloseIcon fontSize="small" />
          </IconButton>
        </InputAdornment>
        </div>
    )
    : (
      <div style={{width: CLEAR_ICON_WIDTH}}/>
    ),
    classes: {
      input: classes.input
    }
  };

  return (
    <Grid container alignItems={"flex-end"}>
        <TextField
          variant={variant}
          label={label}
          placeholder={placeholder}
          fullWidth
          value={fieldValue}
          InputProps={inputProps}
          onChange={e => handleChange(e.target.value)}
          inputProps={{ style: { paddingRight: 0 } }}
          name={name}
          size={'small'}
        />
        {textType && fieldValue.length <= 2 && fieldValue.length > 0 && needCheckLength && (
          <div className="center" style={{ color: 'red' }}>
            {'минимум 3 символа'}
          </div>
        )}
    </Grid>
  );
}

export default FilterInputField;
