import styled from 'styled-components';

export const Wrapper = styled.div`
  .rt-th.rt-resizable-header {
    justify-content: left !important;
    padding-left: 15px !important;
  }
  .rt-thead.-header {
    top: 0;
    //z-index: 200;
    //position: sticky;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    background-color: white;
    border-radius: 4px;
    box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.1), 0px 2px 2px 0px rgba(0, 0, 0, 0.08),
      0px 3px 1px -2px rgba(0, 0, 0, 0.05) !important;
  }
  .rt-thead.-filters {
    position: inherit;
    top: 55px;
    z-index: 150;
    background-color: #f9f9f9;
  }

  .ReactTable {
    position: relative;
    z-index: 0;
    border: none;
    overflow: auto;
    box-shadow: 0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%),
      0px 1px 3px 0px rgb(0 0 0 / 12%);
    border-radius: 8px;

    .rt-thead .rt-tr {
      height: 50px;
      align-items: center;
      //background: darkslategrey;
      color: black;
    }
    .rt-table {
      border: 1px solid rgba(0, 0, 0, 0.1);
      overflow: inherit;
      border-radius: 4px;
    }
    .rt-tbody {
      background-color: white;
      border-bottom-left-radius: 8px;
      border-bottom-right-radius: 8px;
    }

    .rt-pagination {
      display: flex;
      text-align: right;
      justify-content: flex-end;
      margin-top: 10px;
      height: 50px;
    }

    .rt-tr-group {
      cursor: ${props => (props.pointer ? 'pointer' : 'inherit')};
    }

    .rt-resizable-header-content {
      width: 100% !important;
      text-align: justify;
    }
    .rt-th {
      display: flex;
      justify-content: left !important;
      padding: 10px 5px;
    }
    .rt-td {
      display: flex;
      align-items: center !important;
      padding: 10px 5px;
    }
  }
`;

export const Loading = styled.div`
  z-index: 100 !important;
  .-loading-inner {
    //margin-top: -20px;
    .rt-loading-circle {
      padding: 10px;
      width: 61px;
      height: 61px;
      margin: auto;
      border-radius: 50%;
      background-color: white;
      box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.1), 0px 2px 2px 0px rgba(0, 0, 0, 0.08),
        0px 3px 1px -2px rgba(0, 0, 0, 0.05);
    }
  }
`;
