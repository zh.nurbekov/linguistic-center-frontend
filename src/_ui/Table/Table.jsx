import React from 'react';
import ReactTable from 'react-table';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Loading, Wrapper } from './TableStyles';
import Pagination from './Pagination';
import withTable from './withTable';
import { withRouter } from 'react-router-dom';
import usePaginationFilter from './usePaginationFilter';

const Table = props => {
  const { filters, loadData, data, total } = props;

  const { page, setPage, limit, setLimit, offset, setOffset } = usePaginationFilter(
    filters,
    loadData,
    total,
    data
  );

  const customComponents = () => {
    return {
      getTdProps: (tableState, rowInfo, column) => {
        const { onClickTd } = props;
        return {
          onClick(e) {
            if (typeof onClickTd === 'function') onClickTd(tableState, rowInfo, column, e);
          },
          style: {
            cursor: typeof onClickTd === 'function' ? 'pointer' : 'default'
          }
        };
      },
      getTrProps(props, object) {
        return {
          onClick() {
            if (typeof props.onClickRow === 'function') props.onClickRow(object);
          },
          style: {
            cursor: typeof props.onClickRow === 'function' ? 'pointer' : 'default'
          }
        };
      },
      getTheadThProps() {
        return {
          style: {
            justifyContent: 'left',
            padding: '7px 15px'
          }
        };
      },
      PaginationComponent() {
        return (
          <Pagination
            total={total}
            data={data}
            page={page}
            offset={offset}
            setPage={setPage}
            setLimit={setLimit}
            setOffset={setOffset}
            limit={limit}
          />
        );
      },
      NoDataComponent() {
        if (!props.loading) {
          return (
            <Loading className="-loading -active">
              <div className="-loading-inner">{props.noDataText || 'Нет данных'}</div>
            </Loading>
          );
        }
        return null;
      },
      LoadingComponent(data) {
        if (data.loading) {
          return (
            <Loading className="-loading -active">
              <div className="-loading-inner">
                <div className="rt-loading-circle">
                  <CircularProgress />
                </div>
              </div>
            </Loading>
          );
        }
        return null;
      }
    };
  };

  const getData = () => {
    let i = (page - 1) * limit;
    return [
      ...data.map(item => {
        i++;
        return { ...item, rowNum: i };
      })
    ];
  };

  return (
    <Wrapper pointer={typeof props.onClickRow === 'function'}>
      <ReactTable
        {...props}
        columns={props.columns}
        data={getData()}
       // showPagination={!props.hidePagination}
        defaultPageSize={limit}
        pageSize={limit > total ? total || 1 : limit}
        className="-highlight flex-start"
        sortable={false}
        resizable={false}
        {...customComponents()}
      />
    </Wrapper>
  );
};

Table.propTypes = {
  loading: PropTypes.bool,
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  onClickRow: PropTypes.func,
  total: PropTypes.number.isRequired,
  filters: PropTypes.object,
  loadData: PropTypes.func,
  hidePagination: PropTypes.bool,
  refreshPagination: PropTypes.number,
  noDataText: PropTypes.string,
  queryFilterName: PropTypes.string,
  queryDefaultParam: PropTypes.object,

};

export default withRouter(withTable(Table));
