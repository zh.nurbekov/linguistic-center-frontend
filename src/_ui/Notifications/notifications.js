import React from 'react'
import {store} from "react-notifications-component";
import 'react-notifications-component/dist/theme.css'


export const NotificationComponent = {
    success: ({title, message, position = 'top-right', width = 300, duration = 2000}) => (
        store.addNotification({
            content: MyNotification(title, message),
            insert: 'bottom',
            container: position,
            width: width,
            dismiss: {
                duration: duration,
            },
        })),
    danger: ({title, message, position = 'top-right', width = 300, duration = 2000}) => (
        store.addNotification({
            title: title,
            message: message,
            insert: 'bottom',
            type: 'danger',
            container: position,
            width: width,
            dismiss: {
                duration: duration,
            },
        })),
    info: ({title, message, position = 'top-right', width = 300, duration = 2000}) => (
        store.addNotification({
            title: title,
            message: message,
            insert: 'bottom',
            type: 'info',
            container: position,
            width: width,
            dismiss: {
                duration: duration,
            },
        })),
    warning: ({title, message, position = 'top-right', width = 300, duration = 2000}) => (
        store.addNotification({
            title: title,
            message: message,
            insert: 'bottom',
            type: 'warning',
            container: position,
            width: width,
            dismiss: {
                duration: duration,
            },
        })),
    default: ({title, message, position = 'top-right', width = 300, duration = 2000}) => (
        store.addNotification({
            title: title,
            message: message,
            insert: 'bottom',
            type: 'default',
            container: position,
            width: width,
            dismiss: {
                duration: duration,
            },
        })),
};

export function MyNotification(title, message) {
    return (
        <div
            style={{
                display: "flex",
                backgroundColor: "#28a745",
                borderRadius: 3,
                borderLeft: "8px solid #1f8838",
                color: "white",
                boxShadow: "1px 3px 4px rgba(0,0,0,.2)",
                fontSize: "14px",
                maxWidth: "100%",
                width: "100%"
            }}
        >
            <div
                style={{
                    padding: "8px 15px",
                    width: "100%"
                }}
            >
                <div
                    style={{
                        fontWeight: 700,
                        marginTop: "5px",
                        marginBottom: "5px"
                    }}
                >
                    {title}
                </div>
                <div
                    style={{
                        lineHeight: "150%"
                    }}
                >
                    {message}
                </div>
            </div>
        </div>
    );
};
