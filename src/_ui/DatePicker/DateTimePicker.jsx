import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardDateTimePicker } from '@material-ui/pickers';
import * as moment from 'moment';
import Field from '../Form/Field';

function DateTimePicker(props) {
  const { withoutForm, name, onChange, value, ...restProps } = props;

  return withoutForm ? (
    <KeyboardDateTimePicker
      name={name}
      variant="inline"
      disableToolbar
      strictCompareDates
      // format="YYYY-MM-DD HH:mm:ss"
      autoOk
      KeyboardButtonProps={{ style: { marginRight: 0 } }}
      invalidDateMessage={'Недопустимый формат даты'}
      onChange={date => onChange(date ? date.unix() : '')}
      value={value && moment.unix(value)}
      {...restProps}
    />
  ) : (
    <Field name={name}>
      {({ onChange, value, ...fieldProps }) => {
        return (
          <KeyboardDateTimePicker
            name={name}
            variant="inline"
            ampm={false}
            format="YYYY-MM-DD HH:mm:ss"
            KeyboardButtonProps={{ style: { marginRight: 0 } }}
            invalidDateMessage={'Недопустимый формат даты'}
            onChange={date => onChange(date.format('YYYY-MM-DDTHH:mm:ss'))}
            value={value}
            {...restProps}
            {...fieldProps}
          />
        );
      }}
    </Field>
  );
}

DateTimePicker.propsTypes = {
  withoutForm: PropTypes.bool,
  name: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired
};

DateTimePicker.defaultProps = {
  withoutForm: false
};

export default DateTimePicker;
