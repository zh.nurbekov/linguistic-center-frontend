import React from 'react';
import PropTypes from 'prop-types';
import { KeyboardDatePicker } from '@material-ui/pickers';
import * as moment from 'moment';
import {Field} from "formik";

function DatePicker(props) {
  const { withoutForm, name, onChange, value, ...restProps } = props;

  return withoutForm ? (
    <KeyboardDatePicker
      name={name}
      variant="inline"
      disableToolbar
      strictCompareDates
      format="YYYY-MM-DD HH:mm:ss"
      autoOk
      KeyboardButtonProps={{ style: { marginRight: 0 } }}
      invalidDateMessage={'Недопустимый формат даты'}
      onChange={date => onChange(date ? date.unix() : '')}
      value={value && moment.unix(value)}
      {...restProps}
    />
  ) : (
    <Field name={name}>
      {({ onChange, ...fieldProps }) => (
        <KeyboardDatePicker
          name={name}
          variant="inline"
          disableToolbar
          format="YYYY-MM-DD HH:mm:ss"
          autoOk
          KeyboardButtonProps={{ style: { marginRight: 0 } }}
          invalidDateMessage={'Недопустимый формат даты'}
          onChange={date => onChange(date.format('YYYY-MM-DDTHH:mm:ss'))}
          value={value}
          {...restProps}
          {...fieldProps}
        />
      )}
    </Field>
  );
}

DatePicker.propsTypes = {
  withoutForm: PropTypes.bool,
  name: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired
};

DatePicker.defaultProps = {
  withoutForm: false
};

export default DatePicker;
