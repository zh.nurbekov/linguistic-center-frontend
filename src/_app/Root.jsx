import React, { useEffect } from 'react';
import { NotificationContainer } from 'react-notifications';
import ProtectedRoute from './ProtectedRoute';
import Page404 from '../pages/Page404/Page404';
import Page403 from '../pages/Page403/Page403';
import routers from '../_helpers/routers';
import ProgressBar from '../components/ProgressBar/ProgressBar';
import Header from '../components/Header/Header';
import { Route, Switch, useLocation } from 'react-router-dom';
import { localSecureStorage } from './App';
import { loadCheckMessage } from '../pages/Message/MessageDucks';
import { useDispatch, useSelector } from 'react-redux';
import Footer from '../components/Footer/Footer';
import { getRole, loginModule } from '../pages/LoginPage/LoginDucks';

function Root(props) {
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const permissions = useSelector(state => state[loginModule]);
  const user = localSecureStorage.getItem('user');

  useEffect(() => {
    const user = localSecureStorage.getItem('user');
    if (user) {
     dispatch(getRole(user.role));
    }
  }, []);

  useEffect(() => {
    const user = localSecureStorage.getItem('user');
    if (user) {
      dispatch(loadCheckMessage(user.id));
    }
  }, [pathname]);

  return (
    <>
      <NotificationContainer />
      <Route exact path={withoutHeader} {...props} component={Header} />
      <ProgressBar />
      <Switch>
        {routers.map(route => (
          <ProtectedRoute key={route.path} {...route} {...props} />
        ))}
        <Route component={Page404} />
        <Route component={Page403} />
      </Switch>
      <Route exact path={withoutHeader} {...props} component={Footer} />
    </>
  );
}

export const withoutHeader = routers
  .filter(item => !item['withoutHeader'])
  .map(item => item.path);

export default Root;
