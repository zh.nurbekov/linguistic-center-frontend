import React from 'react';
import { Route } from 'react-router-dom';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <>
      <Route
        {...rest}
        render={routeProps => {
          // if (rest.withoutAuth && rest.path !== paths.loginPage) {
          //   return <Component {...routeProps} />;
          // }

          // перенаправляем на страницу логина если не авторизован
          // if (!rest.withoutAuth && !token) {
          //   return history.push(paths.loginPage);
          // }
          //
          // // перенаправляем если авторизован и находися на странице логина
          // if (token && rest.path === paths.loginPage) {
          //   return history.push(paths.main);
          // }

          // if (rest.permissions && !checkPermissions(rest.permissions)) {
          //   return <Page403 {...routeProps} />;
          // }

          return <Component key={routeProps.path} {...routeProps} />;
        }}
      />
    </>
  );
};

export default ProtectedRoute;
