import React from 'react';
import ReactDOM from 'react-dom';
import App from './_app/App';
import * as serviceWorker from './serviceWorker';
import './_css/style.scss';
import './_css/basscss.min.css';
import 'react-table/react-table.css';
import 'react-notifications-component/dist/scss/notification.scss';
import 'react-notifications/lib/notifications.css';
import 'tailwindcss/tailwind.css';
import 'react-chatbox-component/dist/style.css';
import 'antd/dist/antd.css';

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.register();
