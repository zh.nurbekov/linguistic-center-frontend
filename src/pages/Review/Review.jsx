import React, { useEffect, useState } from 'react';
import { Api } from '../../_helpers/service';
import { Card, makeStyles, TextField } from '@material-ui/core';
import { Avatar, Comment, Tooltip, message } from 'antd';
import * as moment from 'moment';
import Button from '../../_ui/Button/Button';
import IconButton from '../../_ui/Button/IconButton';
import SendIcon from '@material-ui/icons/Send';
import { localSecureStorage } from '../../_app/App';
import Warning from './Warning';
import useSimpleModal from '../../components/_hooks/useSimpleModal';

const useStyles = makeStyles({
  root: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

export default function Review() {
  const [reviews, setReviews] = useState([]);
  const [showAddBtn, setShowAddBtn] = useState();
  const [comment, setComment] = useState('');
  const user = localSecureStorage.getItem('user');
  const warningModal = useSimpleModal();

  console.log(reviews);
  const deleteReview = id => {
    Api.delete(`api/remove/review/${id}`)
      .then(({ data }) => {
        message.success('Отзыв успешно удален', 3);
        setReviews([...reviews.filter(item => item.id !== id)]);
      })
      .catch(e => console.log(e));
  };

  useEffect(() => {
    Api.get('api/get/reviews')
      .then(({ data }) => setReviews(data.data))
      .catch(e => console.log(e));
  }, []);

  const onCreate = values => {
    Api.post('api/send/review', { text: comment, userId: user.id })
      .then(({ data }) => {
        console.log(data.message);
        message.success(data.message, 3);
        setReviews([{ userId: user, text: comment, date: moment.now() }, ...reviews]);
        setShowAddBtn(false);
        setComment('');
      })
      .catch(e => console.log(e));
  };

  console.log(user);

  const addReviewInputProps = {
    endAdornment: (
      <div className={'flex items-end justify-end'}>
        <IconButton
          tooltip={'Сохранить'}
          style={{ padding: 2 }}
          onClick={onCreate}
          icon={<SendIcon />}
        />
      </div>
    )
  };

  return (
    <div className="col-12  " style={{ height: '100%' }}>
      <div className="flex justify-center">
        <div className="col-2" />
        <div className=" p4 col-8" style={{ background: 'white' }}>
          <div className="mt2 mr4 flex justify-end">
            <div>
              {user && (
                <Button
                  text={showAddBtn ? 'Отмена' : 'Оставить отзыв'}
                  variant={'standard'}
                  onClick={() => {
                    if (showAddBtn) {
                      setComment('');
                      setShowAddBtn(!showAddBtn);
                    } else {
                      warningModal.open({});
                    }
                  }}
                />
              )}
            </div>
          </div>
          {showAddBtn && (
            <div className="p2">
              <TextField
                value={comment}
                onChange={({ target: { value } }) => setComment(value)}
                multiline
                rows={3}
                fullWidth
                placeholder="Новый отзыв"
                InputProps={addReviewInputProps}
              />
            </div>
          )}

          {reviews.map(item => {
            return (
              <div className="m2 " style={{ marginTop: 10 }}>
                <Card title="Card title" bordered={false} style={{ padding: 10 }}>
                  <Comment
                    author={`${item.userId.firstname} ${item.userId.middlename}`}
                    avatar={
                      <Avatar
                        src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                        alt="Han Solo"
                      />
                    }
                    content={<p>{item.text}</p>}
                    datetime={
                      <Tooltip title={moment(item.date).format('YYYY-MM-DD HH:mm:ss')}>
                        <span>{moment(item.date).format('YYYY-MM-DD HH:mm:ss')}</span>
                      </Tooltip>
                    }
                  />
                  {user.userType === 1 && (
                    <div className="flex justify-end">
                      <Button
                        type="primary"
                        text={'Удалить'}
                        style={{ color: 'red' }}
                        onClick={() => deleteReview(item.id)}
                      />
                    </div>
                  )}
                </Card>
              </div>
            );
          })}
        </div>
        <div className="col-2" />
      </div>
      {warningModal.isOpen && (
        <Warning
          close={warningModal.close}
          okButtonClick={() => {
            setComment('');
            setShowAddBtn(!showAddBtn);
            warningModal.close();
          }}
        />
      )}
    </div>
  );
}
