import React from 'react';
import { Typography, useTheme } from '@material-ui/core';

export default function Answers({ question }) {
  const { palette } = useTheme();
  return (
    <div className="mt2">
      <h3 children={question.name} />
      <div>
        <div>
          {question.image && question.image.indexOf('null') < 0 && (
            <img src={question.image} width={300} />
          )}
          <div className="ml3">
            {question.answerValues.map((item, index) => {
              return question.answerValueCorrectId === item.id ? (
                <Typography
                  children={item.name}
                  style={{
                    color: palette.success.main
                  }}
                />
              ) : (
                <Typography
                  children={item.name}
                  style={{
                    color: item.correct ? palette.error.main : palette.text.primary
                  }}
                />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
