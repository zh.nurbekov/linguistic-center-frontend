import { Api } from '../../_helpers/service';

export const FilingApplicationApi = {
  loadQuestion: (categoryId, userId) => Api.get(`api/start/exam/${categoryId}/${userId}`),
  saveQuestion: questions => Api.post(`api/end/test`, questions),
  loadCategory: () => Api.get('api/categories')
};
