import React, { useEffect, useState } from 'react';
import { DialogContent, Typography, useTheme } from '@material-ui/core';
import Modal from '../../_ui/Modal/Modal';
import { Api } from '../../_helpers/service';
import Answers from './Answers';
import { localSecureStorage } from '../../_app/App';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/history';

export default function Results({ close: closeModal, data }) {
  const [results, setResults] = useState([]);
  const [isCorrectAnswerCount, setIsCorrectAnswerCount] = useState(0);
  const { palette } = useTheme();
  const user = localSecureStorage.getItem('user');

  const close = () => {
    closeModal();
    history.push(paths.homePage);
  };

  useEffect(() => {
    Api.get(`api/test/result/${data.id}`)
      .then(({ data }) => {
        setResults(data);
        const countCorrect = data.filter(({ answerCorrect }) => answerCorrect).length;
        setIsCorrectAnswerCount(countCorrect);
      })
      .catch(e => console.log(e));
  }, []);

  return (
    <Modal
      maxWidth={'lg'}
      title={
        results.length
          ? `Результат тестирование :  ${isCorrectAnswerCount} из ${results.length}`
          : ''
      }
      onClose={close}
    >
      <div className="p3" style={{ background: palette.background.default }}>
        <div className="mb2">
          <Typography variant="h6" children={`${data.firstname} ${data.lastname}`} />
        </div>
        {results.length ? (
          <div className="my2">
            <Typography variant="subtitle2">
              Уровень зависит от набранных балов
              <br /> - Beginner (начальный) 0-4 баллов <br />
              - Elementary (выше начального) 5-9 баллов <br />
              - Pre-Intermediate (средний начальный) 10-13 баллов
              <br />
              - Intermediate (средний) 14-17 баллов <br />
              - Upper-Intermediate (выше среднего) 18-21 баллов <br />
              - Advanced (продвинутый) 22-26 баллов <br />- Proficient 27-30 баллов'
            </Typography>
          </div>
        ) : null}
        <div className="px4 py2">
          <DialogContent className={'pt1'}>
            {results.length ? (
              <div>
                {results.map((item, index) => (
                  <div className="p2 mb2">
                    <Answers question={item} />
                  </div>
                ))}
              </div>
            ) : (
              <div>Тест еще не сдан</div>
            )}
          </DialogContent>
        </div>
      </div>
    </Modal>
  );
}
