import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, useState } from 'react';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import {
  loadCategory,
  loadQuestions,
  PassTestModule,
  saveQuestions,
  setQuestions
} from './PassTestDucks';
import Select from '../../_ui/Select/Select';
import {
  DialogActions,
  DialogContent,
  Paper,
  Typography,
  useTheme
} from '@material-ui/core';
import Question from './Question';
import Button from '@material-ui/core/Button';
import { localSecureStorage } from '../../_app/App';
import Results from './Results';

export default function PassTest() {
  const { category, questions } = useSelector(state => state[PassTestModule]);
  const [currentCategory, setCurrentCategory] = useState('');
  const { palette } = useTheme();
  const dispatch = useDispatch();
  const resultModal = useSimpleModal();
  const user = localSecureStorage.getItem('user');


  useEffect(() => {
    dispatch(loadCategory());
  }, []);

  useEffect(() => {
    currentCategory && dispatch(loadQuestions(currentCategory, user.id));
  }, [currentCategory]);

  const onSave = () => {
    const answers = questions.map(item => ({
      name: item.name,
      user: user.id,
      questionId: item.questionId,
      answerValues: item.questionValue.map(({ correct, name }) => ({ name, correct }))
    }));
    dispatch(saveQuestions(answers, () => resultModal.open(user)));
  };

  const onChange = (answerId, questionId) => {
    const newQuestions = questions.map(item => {
      if (item.id === questionId) {
        const questionValue = item.questionValue.map(question => {
          return { ...question, correct: question.id === answerId };
        });
        return { ...item, questionId: questionId, questionValue, answerId };
      }
      return item;
    });
    dispatch(setQuestions(newQuestions));
  };

  return (
    <div className="p3" style={{ height: '100%' }}>
      <div className="mb2">
        <Typography variant="h6" children={'Пройти тестирование'} />
      </div>
      <Paper>
        <div className="px4 py2">
          <div className="flex justify-between items-center mb2">
            <Typography variant="body1">
              Для проверки уровня зананий выберите пожалуйста категорию и пройдите тест
            </Typography>
            <div>
              <Select
                placeholder={'Выберите категорию'}
                onChange={({ target: { value } }) => setCurrentCategory(value)}
                value={currentCategory}
                style={{ width: 300 }}
                variant={'outlined'}
                withoutForm
                options={category.map(({ id, name }) => ({ value: id, name }))}
              />
            </div>
          </div>
          <DialogContent dividers className={'pt1'}>
            {questions
              .filter(item => item.categoryGroup.id === currentCategory)
              .map((item, index) => (
                <div className="p2 mb2">
                  <Question
                    question={item}
                    onChange={(answerId, questionId) =>
                      onChange(answerId, questionId, index)
                    }
                  />
                </div>
              ))}
          </DialogContent>
          <DialogActions>
            <Button
              style={{ color: 'white', background: palette.error.main }}
              children="Отмена"
              variant={'contained'}
              onClick={close}
            />
            <Button
              onClick={onSave}
              variant={'contained'}
              children={'Завершить'}
              style={{ color: 'white', background: palette.success.main }}
            />
          </DialogActions>
        </div>
      </Paper>
      {resultModal.isOpen && <Results  {...resultModal} />}
    </div>
  );
}
