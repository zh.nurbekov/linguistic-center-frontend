import React from 'react';
import { FormControlLabel, Radio, RadioGroup } from '@material-ui/core';

export default function Question({ question, onChange }) {
  return (
    <div className="mt2">
      <h3 children={question.name} />
      <div>
        <div>
          {question.image && question.image.indexOf('null') < 0 && (
            <img src={question.image} width={300} />
          )}
          <div>
            <RadioGroup
              aria-label="gender"
              value={String(question.answerId)}
              onChange={({ target: { value } }) => {
                onChange(parseInt(value), question.id);
              }}
            >
              {question.questionValue.map(item => {
                return (
                  <FormControlLabel
                    value={String(item.id)}
                    control={<Radio />}
                    label={item.name}
                  />
                );
              })}
            </RadioGroup>
          </div>
        </div>
      </div>
    </div>
  );
}
