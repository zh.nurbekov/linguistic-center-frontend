import { createReducer } from '@reduxjs/toolkit';
import { FilingApplicationApi } from './FilingApplicationService';
import statusMessage from '../../utils/statusMessage';

/**
 * Constants
 */

export const PassTestModule = 'PassTest';
export const QUESTIONS = `${PassTestModule}/QUESTIONS`;
export const CATEGORY = `${PassTestModule}/CATEGORY`;
export const LOADING = `${PassTestModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  questions: [],
  category: [],
  loading: false
};

export default createReducer(initialState, {
  [QUESTIONS]: (state, { payload }) => {
    state.questions = payload;
  },
  [CATEGORY]: (state, { payload }) => {
    state.category = payload;
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const setQuestions = questions => async dispatch => {
  try {
    dispatch({ type: QUESTIONS, payload: questions });
  } catch (e) {
    console.error(e);
  }
};

export const loadQuestions = (categoryId, userId) => async dispatch => {
  try {
    let { data } = await FilingApplicationApi.loadQuestion(categoryId, userId);
    const start = () => {
      const questions = data.data.map(question => {
        const questionValue = question.questionValue.map(item => ({
          ...item,
          correct: false
        }));
        return { ...question, questionValue };
      });
      dispatch({ type: QUESTIONS, payload: questions });
    };
    statusMessage(data, start);
  } catch (e) {
    console.error(e);
  }
};

export const saveQuestions = (questions, resultModal) => async dispatch => {
  try {
    let { data } = await FilingApplicationApi.saveQuestion(questions);
    if (data.status === 'SUCCESS') {
      resultModal && resultModal();
    }
    //dispatch({ type: QUESTIONS, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const loadCategory = () => async dispatch => {
  try {
    let { data } = await FilingApplicationApi.loadCategory();
    dispatch({ type: CATEGORY, payload: data });
  } catch (e) {
    console.error(e);
  }
};
