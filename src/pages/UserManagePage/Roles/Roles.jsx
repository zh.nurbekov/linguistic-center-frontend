import React from 'react';
import { roleColumns } from './RoleUtils';
import { getRoles, rolesModule, updateRoles } from './RolesDucks';
import { useDispatch, useSelector } from 'react-redux';
import useSimpleModal from '../../../components/_hooks/useSimpleModal';
import Table from "../../../_ui/Table/Table";

const roleFilterName = '_role';
function Roles({createModal}) {
  const { roles, loading } = useSelector(state => state[rolesModule]);
  const dispatch = useDispatch();
  const update = role => dispatch(updateRoles(role));

  return (
    <div className="px4 py3" style={{height:'100%'}}>
      <Table
        queryFilterName={roleFilterName}
        columns={roleColumns(update, createModal.open)}
        data={roles.content}
        loading={false}
        total={roles.content.length}
        loadData={filter => dispatch(getRoles(filter))}
      />
    </div>
  );
}

export default Roles;
