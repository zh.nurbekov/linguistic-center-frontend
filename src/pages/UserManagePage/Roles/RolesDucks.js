import { createReducer } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import { RolesApi } from './RolesService';
import {
  hideProgress,
  showProgress
} from '../../../components/ProgressBar/ProgressBarDucks';

/**
 * Constants
 */

export const rolesModule = 'roles';
export const ROLES = `${rolesModule}/ROLES`;
export const CREATE = `${rolesModule}/CREATE`;
export const UPDATE = `${rolesModule}/UPDATE`;
export const LOADING = `${rolesModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  roles: { content: [], count: 0 },
  loading: false
};

export default createReducer(initialState, {
  [ROLES]: (state, { payload: { data: content, count } }) => {
    state.roles = { content, count };
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.roles.content = [payload, ...state.roles.content];
    state.roles.count = state.roles.count + 1;
  },
  [UPDATE]: (state, { payload }) => {
    state.roles.content = [
      ...state.roles.content.map(item => (item.id === payload.id ? payload : item))
    ];
  },
  [LOADING]: (state, action) => {
    state.loading = action.loading;
  }
});

/**
 * Actions
 */

export const getRoles = () => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let data = await RolesApi.getRoles();
    dispatch({ type: ROLES, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const createRoles = (role, modalOnClose) => async dispatch => {
  try {
    dispatch(showProgress());
    console.log(role);
    role.access_list = JSON.stringify(role.access_list);
    let { data } = await RolesApi.createRoles(role);
    dispatch({ type: CREATE, payload: data });
    NotificationManager.success('Данные успешно сохранены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const updateRoles = (role, modalOnClose) => async dispatch => {
  try {
    console.log(role.access_list)
    dispatch(showProgress());
    role.access_list = JSON.stringify(role.access_list);
    let { data } = await RolesApi.updateRole(role);
    dispatch({ type: UPDATE, payload: data });
    NotificationManager.success('Данные успешно сохранены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    console.error(e);
    dispatch(hideProgress());
  }
};
