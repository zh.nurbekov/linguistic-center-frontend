import React from 'react';
import * as yup from 'yup';
import { Tooltip } from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';
import Button from '@material-ui/core/Button';

//для формы валидация полей
export const RolesValidate = yup.object().shape({
  name: yup.string().required('Обязательное поле для заполнения')
});

// для таблицы загаловки и т.д
export const roleColumns = (update, modalOnOpen) => {
  const columns = [
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      Cell: ({ original: { rowNum } }) => (
        <div className="flex justify-center col-12">{rowNum}</div>
      )
    },
    { Header: 'Наименование', accessor: 'name' }
  ];

  const actions = {
    Header: 'Действие',
    accessor: 'is_active',
    width: 250,

    Cell: ({ original }) => (
      <div className="center">
        <Button
          className="ml1"
          type={'link'}
          size={'small'}
          color="primary"
          children={'Редактировать'}
          // children={<CreateIcon />}
          onClick={() => modalOnOpen(original)}
        />
      </div>
    )
  };

  columns.push(actions);

  return columns;
};
