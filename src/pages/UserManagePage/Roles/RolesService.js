import { Api } from '../../../_helpers/service';

export const RolesApi = {
  getRoles: () => Api.get('api/roles'),
  getRole: id => Api.get(`api/role/${id}`),
  createRoles: role => Api.post('api/create/role', role),
  updateRole: role => Api.put(`api/update/role`, role)
};
