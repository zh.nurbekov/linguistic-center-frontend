import React, { useState } from 'react';
import { RolesValidate } from './RoleUtils';
import { useDispatch } from 'react-redux';
import Modal from '../../../_ui/Modal/Modal';
import Input from '../../../_ui/Input/Input';
import {
  Checkbox,
  DialogActions,
  DialogContent,
  FormControlLabel,
  useTheme
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { PERMISSION_NAMES, PERMISSIONS } from '../../../_helpers/permissions';
import Form from '../../../_ui/Form/Form';
import { createRoles, updateRoles } from './RolesDucks';

const getPermissions = values => {
  try {
    return values ? JSON.parse(values) : [];
  } catch (e) {
    console.log(e);
  }
};

function RolesEditModal({ data, close: modalOnClose, dataIsEmpty }) {
  const [accessList, setAccessList] = useState(getPermissions(data?.access_list ));
  const { palette } = useTheme();
  const dispatch = useDispatch();

  console.log(accessList);
  const onSubmit = values => {
    const formData = { ...values, access_list: accessList };
    dataIsEmpty
      ? dispatch(createRoles(formData, modalOnClose))
      : dispatch(updateRoles(formData, modalOnClose));
  };

  const onChangeAccess = (access, checked) => {
    const list = checked
      ? [...accessList, access]
      : [...accessList.filter(item => item !== access)];
    console.log(list, access, checked);
    setAccessList(list);
  };

  return (
    <div>
      <Modal
        fullScreen
        title={dataIsEmpty ? 'Добавление роли' : 'Редактирование роли'}
        onClose={() => modalOnClose()}
      >
        <Form initialValues={data} validate={RolesValidate} onSubmit={onSubmit}>
          <DialogContent dividers className={'pt1'}>
            <Input fullWidth name="name" label={'Наименование*'} />
            <div className="my2">
              <div children={'Права доступа'} className="my2" />
              {Object.values(PERMISSIONS).map(item => {
                return (
                  <div>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={accessList.includes(item)}
                          onChange={({ target: { checked } }) =>
                            onChangeAccess(item, checked)
                          }
                          color="primary"
                        />
                      }
                      label={PERMISSION_NAMES[item]}
                    />
                  </div>
                );
              })}
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              style={{ color: 'white', background: palette.error.main }}
              children="Отмена"
              variant={'contained'}
              onClick={close}
            />
            <Button
              type="submit"
              variant={'contained'}
              children={dataIsEmpty ? 'Добавить' : 'Сохранить'}
              style={{ color: 'white', background: palette.success.main }}
            />
          </DialogActions>
        </Form>
      </Modal>
    </div>
  );
}

export default RolesEditModal;
