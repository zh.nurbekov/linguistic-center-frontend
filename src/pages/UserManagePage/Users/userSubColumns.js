import React from 'react';

export const userSubColumns = () => {
  const columns = [
    {
      Header: 'Имя',
      accessor: 'firstname',
      Cell: ({ original: { firstname } }) => <div className="pl2">{firstname}</div>
    },
    { Header: 'Фамилия', accessor: 'middlename', filterable: false },
    { Header: 'Контакты', accessor: 'lastname', filterable: false },
    { Header: 'Контакты', accessor: 'gender', filterable: false },
    { Header: 'Дата рождений', accessor: 'birthdate', filterable: false }
  ];

  return columns;
};
