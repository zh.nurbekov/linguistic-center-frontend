import { Api } from '../../../_helpers/service';
import axios from 'axios';

export const UsersApi = {
  loadCategory: () => Api.get('api/categories'),
  loadUserType: () => Api.get('api/usertypes'),
  createChildren: values => Api.post(`api/create/children`, values),
  updateUser: user => Api.put(`api/update/user`, user),
  deleteUser: userId => Api.delete(`api/delete/user/${userId}`),
  createUser: user => Api.post('api/registr', user),
  getRoles: () => Api.get('api/roles'),
  getUsers: params => {
    console.log(params,'=====>')
    let data = new FormData();
    const json = JSON.stringify(params);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('filter', blob);
    return axios.post('api/search/users', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
