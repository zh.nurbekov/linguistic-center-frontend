import React, { useEffect } from 'react';
import {
  deleteUser,
  loadCategory,
  loadRoles,
  loadUsers,
  loadUserType,
  usersModule
} from './UsersDucks';
import { userColumns } from './UserColumns';
import { useDispatch, useSelector } from 'react-redux';
import Table from '../../../_ui/Table/Table';
import getOptions from '../../../utils/getOptions';
import FilterSelect from '../../../components/TableFilter/ColumnsFilters/FilterSelect';
import FilterInputField from '../../../components/TableFilter/ColumnsFilters/FilterInputField';
import useQueryFilter from '../../../components/Filters/useQueryFilter';
import Button from '../../../_ui/Button/Button';
import { Wrapper } from '../../Groups/GroupStyles';
import ReactTable from 'react-table';
import useTableExpander from '../../Groups/useTableExpander';
import { userSubColumns } from './userSubColumns';
import ChildrenCreateModal from './ChildrenCreateModal';
import useSimpleModal from '../../../components/_hooks/useSimpleModal';
import { Api } from '../../../_helpers/service';
import Results from '../../PassTest/Results';

export const userFilterName = '_user';
function Users({ createModal }) {
  const { expanded, editExpanded } = useTableExpander(userFilterName);
  const childrenModal = useSimpleModal();
  const resultModal = useSimpleModal();

  const { users, usersCount, category, userType, roles } = useSelector(
    state => state[usersModule]
  );
  const { reset } = useQueryFilter({ index: userFilterName });
  const dispatch = useDispatch();

  const removeUser = user => dispatch(deleteUser(user));

  useEffect(() => {
    dispatch(loadRoles());
    dispatch(loadUserType());
    dispatch(loadCategory());
  }, []);

  const loadContract = userId => {
    Api.get(`api/genered/report/contract/${userId}`)
      .then(data => console.log(data))
      .catch(e => console.log(e));
  };

  return (
    <div className="px4 py2" style={{ height: '100%' }}>
      <div className="flex justify-between items-center">
        <div className="flex  items-end py1">
          <div style={{ width: 200, paddingTop: 20 }} className="px ">
            <FilterInputField
              name={'firstname'}
              filterName={userFilterName}
              placeholder={'Имя'}
            />
          </div>
          <div style={{ width: 200 }} className="px1">
            <FilterSelect
              placeholder={'Тип пользователя'}
              name={'userType'}
              filterName={userFilterName}
              options={getOptions(userType)}
            />
          </div>
        </div>
        <Button text={'Сбросить'} onClick={() => reset()} variant={'outlined'} />
      </div>
      <Table
        expanded={expanded}
        queryFilterName={userFilterName}
        columns={userColumns(
          userType,
          category,
          createModal.open,
          editExpanded,
          childrenModal.open,
          roles,
          removeUser,
          loadContract,
          resultModal
        )}
        data={users}
        loading={false}
        total={usersCount}
        loadData={filter => dispatch(loadUsers(filter))}
        SubComponent={({ original }) => {
          return (
            <Wrapper>
              <div className="p2">
                <ReactTable
                  TheadComponent={() => null}
                  style={{ border: 'none' }}
                  data={original.childrens}
                  columns={userSubColumns()}
                  defaultPageSize={original.childrens.length}
                  showPagination={false}
                />
              </div>
            </Wrapper>
          );
        }}
      />
      {childrenModal.isOpen && <ChildrenCreateModal {...childrenModal} />}
      {resultModal.isOpen && <Results {...resultModal} />}
    </div>
  );
}

export default Users;
