import React from 'react';
import * as yup from 'yup';
import Button from '../../../_ui/Button/Button';
import MuiIconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/ArrowDropDown';
import DiscloseIcon from '@material-ui/icons/ArrowRight';
import { IconButton, Tooltip } from '@material-ui/core';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { STUDENT_TYPE } from '../../../_helpers/constants';
import EditIcon from '@material-ui/icons/Edit';

//для формы валидация полей
export const UsersValidate = yup.object().shape({
  firstname: yup.string().required('Обязательное поле для заполнения'),
  middlename: yup.string().required('Обязательное поле для заполнения'),
  username: yup.string().required('Обязательное поле для заполнения'),
  password: yup.string().required('Обязательное поле для заполнения'),
  birthdate: yup.string().required('Обязательное поле для заполнения'),
  email: yup.string().required('Обязательное поле для заполнения'),
  role: yup.number().required('Обязательное поле для заполнения'),
  gender: yup.string().required('Обязательное поле для заполнения'),
  phone: yup.string().required('Обязательное поле для заполнения'),
  userType: yup.number().required('Обязательное поле для заполнения')
});

export const userColumns = (
  userTypeList,
  categoryList,
  modalIsOpen,
  editExpanded,
  childrenModalOnOpen,
  roles,
  deleteUser,
  loadContract,
  resultModal
) => {
  const Expander = ({ original, isExpanded, index, editExpanded }) => {
    if (original['childrens'] && original['childrens'].length) {
      return (
        <MuiIconButton
          size="small"
          children={isExpanded ? <CloseIcon /> : <DiscloseIcon />}
          onClick={e => {
            e.stopPropagation();
            editExpanded(isExpanded, index, original);
          }}
        />
      );
    }
    return null;
  };

  const columns = [
    {
      Header: '',
      expander: true,
      width: 50,
      Expander: eProps => (
        <div className="flex items-center">
          <div style={{ width: 32, height: 30 }}>
            <Expander {...eProps} editExpanded={editExpanded} />
          </div>
        </div>
      )
    },
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      Cell: ({ original: { rowNum } }) => (
        <span className="flex justify-center col-12">{rowNum}</span>
      ),
      filterable: false
    },
    {
      Header: 'Имя',
      accessor: 'firstname'
    },
    { Header: 'Фамилия', accessor: 'middlename', filterable: false },
    {
      Header: 'Роль',
      accessor: 'roleName',
      Cell: ({ original }) => {
        const roleName = roles.find(item => item.id === original.role);
        return <div>{roleName ? roleName.name : ''}</div>;
      }
    },
    {
      Header: 'Категория группы',
      accessor: 'categoryName',
      Cell: ({ original: { category } }) => {
        const categoryData = categoryList.find(({ id }) => id === category);
        return category ? <span>{categoryData ? categoryData.name : ''}</span> : null;
      }
    },
    {
      Header: 'Тип пользователя',
      accessor: 'userTypeName',
      Cell: ({ original: { userType } }) => {
        const userTypeData = userTypeList.find(({ id }) => id === userType);
        return userType ? <span>{userTypeData ? userTypeData.name : ''}</span> : null;
      }
    },
    {
      Header: 'О себе',
      accessor: 'information',
      filterable: false
    },
    {
      Header: '',
      width: 250,
      accessor: 'action',
      Cell: ({ original }) => {
        return (
          <div className="flex justify-center col-12">
            <div>
              <div>
                <div className="flex justify-center">
                  <Button
                    text={'Договор'}
                    onClick={e => {
                      e.stopPropagation();
                      loadContract(original.id);
                    }}
                  />
                </div>
                <div className="col-12 flex justify-center">
                  <Button
                    text={'Редактироват'}
                    onClick={e => {
                      e.stopPropagation();
                      modalIsOpen(original);
                    }}
                  />
                </div>
                <div className="flex justify-center">
                  <Button text={'Удалить'} onClick={() => deleteUser(original)} />
                </div>
              </div>
              <div>
                {STUDENT_TYPE === original.userType && (
                  <div
                    className="col-12 flex justify-center"
                    onClick={() => childrenModalOnOpen(original.id)}
                  >
                    <Button
                      text={
                        original.childrens ? 'Редактировать ребенка' : 'Добавить ребенка'
                      }
                      onClick={e => {
                        e.stopPropagation();
                        childrenModalOnOpen(original);
                      }}
                    />
                  </div>
                )}
              </div>
              {STUDENT_TYPE === original.userType && (
                <div className="col-12 flex justify-center">
                  <Button
                    text={'Результаты теста'}
                    onClick={e => {
                      e.stopPropagation();
                      resultModal.open(original);
                    }}
                  />
                </div>
              )}
            </div>
          </div>
        );
      }
    }
  ];

  return columns;
};
