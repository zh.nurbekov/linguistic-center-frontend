import React from 'react';
import { createChildren, createUser, usersModule } from './UsersDucks';
import { UsersValidate } from './UserColumns';
import { useDispatch, useSelector } from 'react-redux';
import Modal from '../../../_ui/Modal/Modal';
import Input from '../../../_ui/Input/Input';
import Select from '../../../_ui/Select/Select';
import { Male } from '../../../_helpers/constants';
import Form from '../../../_ui/Form/Form';
import Button from '@material-ui/core/Button';
import { DialogActions, DialogContent, useTheme } from '@material-ui/core';
import * as yup from 'yup';

//для формы валидация полей
export const Validate = yup.object().shape({
  firstname: yup.string().required('Обязательное поле для заполнения'),
  middlename: yup.string().required('Обязательное поле для заполнения'),
  birthdate: yup.string().required('Обязательное поле для заполнения'),
  gender: yup.string().required('Обязательное поле для заполнения')
});

function ChildrenCreateModal({ data, close, dataIsEmpty }) {
  const dispatch = useDispatch();
  const { palette } = useTheme();

  const onSubmit = values => {
    dispatch(createChildren({ ...values, parent: data.id }, close));
  };

  console.log(data.childrens);
  return (
    <div>
      <Modal
        maxWidth={'lg'}
        title={dataIsEmpty ? 'Добавление ребенка' : 'Редактировать ребенка'}
        onClose={() => close()}
      >
        <Form
          validate={Validate}
          initialValues={data.childrens[0]}
          onSubmit={onSubmit}
          render={() => (
            <>
              <DialogContent dividers className={'pt1'}>
                <div className="p2 flex flex-wrap">
                  <div className="col-4 p1">
                    <Input fullWidth label="Фамилия" name="middlename" />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Имя" name="firstname" />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Отчество" name="lastname" />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Дата рождения" name="birthdate" />
                  </div>
                  <div className="col-4 p1">
                    <Select
                      fullWidth
                      label={'Пол'}
                      variant={'outlined'}
                      options={Male}
                      name={'gender'}
                    />
                  </div>
                </div>
              </DialogContent>
              <DialogActions>
                <Button
                  style={{ color: 'white', background: palette.error.main }}
                  children="Отмена"
                  variant={'contained'}
                  onClick={close}
                />
                <Button
                  type="submit"
                  variant={'contained'}
                  children={dataIsEmpty ? 'Создать' : 'Сохранить'}
                  style={{ color: 'white', background: palette.success.main }}
                />
              </DialogActions>
            </>
          )}
        />
      </Modal>
    </div>
  );
}

export default ChildrenCreateModal;
