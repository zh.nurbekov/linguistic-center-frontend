import React from 'react';
import { createUser, updateUser, usersModule } from './UsersDucks';
import { UsersValidate } from './UserColumns';
import { useDispatch, useSelector } from 'react-redux';
import Modal from '../../../_ui/Modal/Modal';
import Input from '../../../_ui/Input/Input';
import Select from '../../../_ui/Select/Select';
import { Male } from '../../../_helpers/constants';
import Form from '../../../_ui/Form/Form';
import Button from '@material-ui/core/Button';
import { DialogActions, DialogContent, useTheme } from '@material-ui/core';
import getOptions from '../../../utils/getOptions';

function UsersCreateModal({ data, close, dataIsEmpty }) {
  const { roles, category, userType } = useSelector(state => state[usersModule]);
  const dispatch = useDispatch();
  const { palette } = useTheme();

  console.log(data, userType);

  const onSubmit = values => {
    dataIsEmpty
      ? dispatch(createUser(values, close))
      : dispatch(updateUser(values, close));
  };

  return (
    <div>
      <Modal
        fullScreen
        // maxWidth={'lg'}
        title={dataIsEmpty ? 'Добавление пользователя' : 'Редактировать пользователя'}
        onClose={() => close()}
      >
        <Form
          validate={UsersValidate}
          initialValues={data}
          onSubmit={onSubmit}
          render={({ userType: type }) => (
            <>
              <DialogContent dividers className={'pt1'}>
                <div className="p2 flex flex-wrap">
                  <div className="col-4 p1">
                    <Input fullWidth label="Фамилия" name="middlename" />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Имя" name="firstname"  />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Отчество" name="lastname" />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Дата рождения" name="birthdate" />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Логин" name={'username'} />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Пароль" name={'password'} />
                  </div>
                  <div className="col-4 p1">
                    <Input fullWidth label="Почта" name={'email'} />
                  </div>

                  <div className="col-4 p1">
                    <Input fullWidth label="Контактные данные" name={'phone'} />
                  </div>
                  <div className="col-4 p1">
                    <Select
                      fullWidth
                      label={'Тип пользователья'}
                      options={getOptions(userType)}
                      name={'userType'}
                    />
                  </div>
                  {type && type === 3 && (
                    <div className="col-4 p1">
                      <Select
                        fullWidth
                        label={'Категоря группы'}
                        options={getOptions(category)}
                        name={'category'}
                      />
                    </div>
                  )}
                  <div className="col-4 p1">
                    <Select
                      fullWidth
                      label={'Роль'}
                      options={getOptions(roles)}
                      name={'role'}
                    />
                  </div>
                  <div className="col-4 p1">
                    <Select
                      fullWidth
                      label={'Пол'}
                      options={Male}
                      name={'gender'}
                    />
                  </div>
                    <div className="col-12 p1">
                      <Input
                        multiline
                        rows={3}
                        fullWidth
                        label="Дополнительная информация"
                        name={'information'}
                      />
                    </div>
                </div>
              </DialogContent>
              <DialogActions>
                <Button
                  style={{ color: 'white', background: palette.error.main }}
                  children="Отмена"
                  variant={'contained'}
                  onClick={close}
                />
                <Button
                  type="submit"
                  variant={'contained'}
                  children={'Создать'}
                  style={{ color: 'white', background: palette.success.main }}
                />
              </DialogActions>
            </>
          )}
        />
      </Modal>
    </div>
  );
}

export default UsersCreateModal;
