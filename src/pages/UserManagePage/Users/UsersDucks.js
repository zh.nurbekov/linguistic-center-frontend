import { createReducer } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import { UsersApi } from './UsersService';
import {
  hideProgress,
  showProgress
} from '../../../components/ProgressBar/ProgressBarDucks';
import statusMessage from '../../../utils/statusMessage';

/**
 * Constants
 */

export const usersModule = 'users';
export const USERS = `${usersModule}/USERS`;
export const CATEGORY = `${usersModule}/CATEGORY`;
export const USER_TYPE = `${usersModule}/USER_TYPE`;
export const CREATE_CHILDREN = `${usersModule}/CREATE_CHILDREN`;
export const ROLES = `${usersModule}/ROLES`;
export const CREATE = `${usersModule}/CREATE`;
export const UPDATE = `${usersModule}/UPDATE`;
export const DELETE = `${usersModule}/DELETE`;
export const SET_FILTER = `${usersModule}/SET_FILTER `;
export const LOADING = `${usersModule}/LOADING `;
export const CLEAR = `${usersModule}/CLEAR `;

/**
 * Reducer
 */

const initialState = {
  users: [],
  usersCount: 0,
  roles: [],
  userFilter: { limit: 20, offset: 0 },
  category: [],
  userType: [],
  loading: false
};

export default createReducer(initialState, {
  [USERS]: (state, { payload: { data, count } }) => {
    state.users = data;
    state.usersCount = count;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.users = [payload, ...state.users];
    state.usersCount = state.usersCount + 1;
  },
  [CREATE_CHILDREN]: (state, { payload }) => {
    state.users = [
      ...state.users.map(item => {
        return item.id === payload.parent
          ? { ...item, childrens: [...item.childrens, payload] }
          : item;
      })
    ];
  },
  [UPDATE]: (state, { payload }) => {
    state.users = [...state.users.map(item => (item.id === payload.id ? payload : item))];
  },
  [DELETE]: (state, { payload }) => {
    state.users = [...state.users.filter(({ id }) => id !== payload.id)];
  },
  [ROLES]: (state, { payload }) => {
    state.roles = payload;
  },
  [CATEGORY]: (state, { payload }) => {
    state.category = payload;
  },
  [USER_TYPE]: (state, { payload }) => {
    state.userType = payload;
  },
  [SET_FILTER]: (state, { payload }) => {
    state.userFilter = payload;
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  },
  [CLEAR]: () => initialState
});

/**
 * Actions
 */

export const clear = () => ({ type: CLEAR });

export const loadUsers = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await UsersApi.getUsers(filter);
    dispatch({ type: USERS, payload: data });
    dispatch({ type: SET_FILTER, payload: filter });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const loadRoles = () => async dispatch => {
  try {
    let { data: roles } = await UsersApi.getRoles();
    dispatch({ type: ROLES, payload: roles });
  } catch (e) {
    console.error(e);
  }
};

export const loadCategory = () => async dispatch => {
  try {
    let { data } = await UsersApi.loadCategory();
    dispatch({ type: CATEGORY, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const loadUserType = () => async dispatch => {
  try {
    let { data } = await UsersApi.loadUserType();
    dispatch({ type: USER_TYPE, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const createChildren = (values, modalClose) => async dispatch => {
  try {
    let { data } = await UsersApi.createChildren(values);
    const setChildren = () => {
      dispatch({ type: CREATE_CHILDREN, payload: values });
      modalClose && modalClose();
    };
    statusMessage(data, setChildren);
  } catch (e) {
    console.error(e);
  }
};

export const createUser = (user, modalOnClose) => async (dispatch, getState) => {
  try {
    dispatch(showProgress());
    let { roles } = getState()[usersModule];
    const { data } = await UsersApi.createUser(JSON.stringify(user));
    const success = () => {
      user.roleName = roles.find(item => item.id === user.role).name;
      dispatch({ type: CREATE, payload: user });
      modalOnClose && modalOnClose();
      console.log('==>');
    };
    statusMessage(data, success);
    dispatch(hideProgress());
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const updateUser = (user, modalOnClose) => async (dispatch, getState) => {
  try {
    dispatch(showProgress());
    let { roles } = getState()[usersModule];
    await UsersApi.updateUser(user);
    console.log(user);
    dispatch({ type: UPDATE, payload: user });
    NotificationManager.success('Данные успешно сохранены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};

export const deleteUser = (user, modalOnClose) => async (dispatch, getState) => {
  try {
    dispatch(showProgress());
    let { roles } = getState()[usersModule];
    let {
      data: { data }
    } = await UsersApi.deleteUser(user.id);
    console.log(data);
    dispatch({ type: DELETE, payload: user });
    NotificationManager.success('Данные успешно удалены');
    dispatch(hideProgress());
    modalOnClose && modalOnClose();
  } catch (e) {
    dispatch(hideProgress());
    console.error(e);
  }
};
