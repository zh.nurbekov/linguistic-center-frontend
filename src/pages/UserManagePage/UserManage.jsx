import React from 'react';
import { Paper, Tab, Tabs, Typography } from '@material-ui/core';
import { Link, useLocation } from 'react-router-dom';
import paths from '../../_helpers/paths';
import { useTheme } from '@material-ui/core/styles';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import Users from './Users/Users';
import Roles from './Roles/Roles';
import Button from '../../_ui/Button/Button';
import UserAddIcon from '@material-ui/icons/PersonAdd';
import AddIcon from '@material-ui/icons/Add';
import UsersCreateModal from './Users/UsersCreateModal';
import RolesEditModal from './Roles/RolesEditModal';

const UserManage = () => {
  const { pathname } = useLocation();
  const { palette } = useTheme();
  const usersUrl = paths.users;
  const rolesUrl = paths.roles;
  const isUserTab = pathname === usersUrl;
  const isRoleTab = pathname === rolesUrl;
  const roleTabColor = isRoleTab ? palette.primary.main : palette.text.secondary;
  const userTabColor = isUserTab ? palette.primary.main : palette.text.secondary;
  const userCreateModal = useSimpleModal();
  const roleCreateModal = useSimpleModal();

  return (
    <div className="p3">
      <div className="mb2">
        <Typography variant="h6" children={'Управление пользователями'} />
      </div>
      <Paper>
        <div className="p3">
          <div className="flex items-center justify-between col-12">
            <Tabs
              indicatorColor={'primary'}
              TabIndicatorProps={{ style: { height: 4 } }}
              value={pathname}
            >
              <Tab
                style={{ color: userTabColor }}
                label={'Пользователи'}
                value={usersUrl}
                component={Link}
                to={usersUrl}
              />
              <Tab
                style={{ color: roleTabColor }}
                label={'Роли'}
                value={rolesUrl}
                component={Link}
                to={rolesUrl}
              />
            </Tabs>
            <div>
              {isUserTab && (
                <Button
                  variant={'contained'}
                  color={'primary'}
                  text={'ДОБАВИТЬ'}
                  icon={<UserAddIcon style={{ marginRight: 10 }} />}
                  onClick={() => userCreateModal.open({})}
                />
              )}
              {isRoleTab && (
                <Button
                  variant={'contained'}
                  color={'primary'}
                  text={'ДОБАВИТЬ'}
                  icon={<AddIcon style={{ marginRight: 10 }} />}
                  onClick={() => roleCreateModal.open({})}
                />
              )}
            </div>
          </div>
        </div>
        <div>
          {isUserTab && <Users createModal={userCreateModal} />}
          {isRoleTab && <Roles createModal={roleCreateModal} />}
        </div>
        {userCreateModal.isOpen && <UsersCreateModal {...userCreateModal} />}
        {roleCreateModal.isOpen && <RolesEditModal {...roleCreateModal} />}
      </Paper>
    </div>
  );
};

export default UserManage;
