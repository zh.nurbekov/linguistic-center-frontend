import React from 'react';
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Divider,
  makeStyles,
  Paper,
  Typography
} from '@material-ui/core';
import { Wrapper } from '../HomePage/HomePageStyles';

const useStyles = makeStyles({
  root: {
    maxWidth: 345
  },
  media: {
    height: 400
  }
});

export default function Achievements() {
  const classes = useStyles();
  return (
    <Wrapper>
      <div className="flex justify-center">
        <Typography style={{ width: 900, marginBottom: 20 }}>
          В этом году сдавали РТЕ немного людей, всего двое. НО! Они к нам ходили всего
          год, а какие результаты . Поздравляем еще раз Айбасову Дану и Нурманова Ерсына{' '}
          <br />
          <br />
          PTE — Pearson Tests of English — линейка международных экзаменов,
          ориентированных на иностранных кандидатов, которые не являются носителями
          английского языка. Тест оценивает, насколько хорошо аппликант владеет
          английским: понимает носителей языка и грамотно выражает свои мысли в устной и
          письменной форме. Экзамен PTE признан во многих странах мира, он является
          наглядным подтверждением текущего уровня знаний кандидата. <br />
          <br />
          Сертификаты PTE принимаются англоязычными колледжами и университетами, а также
          международными компаниями при трудоустройстве на работу за границей. Они не
          имеют срока действия, что является весомым преимуществом экзаменов PTE.(фото в
          папке)
        </Typography>
      </div>

      <div className="col-12   flex justify-center mt2">
        <div style={{ width: 900 }}>
          <div className=" flex ">
            <div style={{ width: 300, marginRight: 30 }}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="/d1.jpg"
                    title="Contemplative Reptile"
                  />
                </CardActionArea>
              </Card>
            </div>
            <div style={{ width: 300, marginRight: 30 }}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="/d2.jpg"
                    title="Contemplative Reptile"
                  />
                </CardActionArea>
              </Card>
            </div>
            <div style={{ width: 300, marginRight: 30 }}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="/d3.jpg"
                    title="Contemplative Reptile"
                  />
                </CardActionArea>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
}
