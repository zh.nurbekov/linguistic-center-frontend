import React, { useEffect } from 'react';
import { Divider, Paper, Typography } from '@material-ui/core';
import { Wrapper } from './HomePageStyles';
import { localSecureStorage } from '../../_app/App';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/history';

export default function HomePage() {
  const user = localSecureStorage.getItem('user');

  useEffect(() => {
    user && history.push(paths.schedulesPage);
  }, []);

  return (
    <Wrapper>
      <div className="col-12   " style={{ height: '100%' }}>
        <Typography
          variant={'h5'}
          children={'Happy Halloween'}
          style={{ marginLeft: 25, color: 'cadetblue' }}
        />
        <Divider style={{ marginTop: 10, marginBottom: 10 }} />
        <div className="col-12 flex flex-wrap justify-around mt2">
          <Paper style={{ padding: 10 }}>
            <img src="/hh1.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10 }}>
            <img src="/hh3.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10 }}>
            <img src="/hh4.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10 }}>
            <img src="/hh5.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10 }}>
            <img src="/hh6.jpg" alt="" width={200} />
          </Paper>
        </div>

        <Typography
          variant={'h5'}
          children={'Наши самые маленькие ученики уже пишут и читают '}
          style={{ marginLeft: 25, color: 'cadetblue', marginTop: 50 }}
        />
        <Divider style={{ marginTop: 10, marginBottom: 10 }} />
        <div className="col-12 flex flex-wrap justify-center  mt2">
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd1.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd2.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd3.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd4.jpg" alt="" width={200} />
          </Paper>
        </div>
        <div className="col-12 flex  justify-center mt2">
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd5.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd6.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd7.jpg" alt="" width={200} />
          </Paper>
        </div>
        <div className="col-12 flex  justify-center mt2">
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd8.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd9.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/rd10.jpg" alt="" width={200} />
          </Paper>
        </div>

        <Typography
          variant={'h5'}
          children={'Ярмарка НГ'}
          style={{ marginLeft: 25, color: 'cadetblue', marginTop: 50 }}
        />
        <Divider style={{ marginTop: 10, marginBottom: 10 }} />
        <div className="col-12 flex justify-around  mt2">
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/ng1.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/ng2.jpg" alt="" width={200} />
          </Paper>

          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/ng4.jpg" alt="" width={200} />
          </Paper>
          <Paper style={{ padding: 10, marginRight: 20 }}>
            <img src="/ng5.jpg" alt="" width={200} />
          </Paper>
        </div>
      </div>
    </Wrapper>
  );
}
