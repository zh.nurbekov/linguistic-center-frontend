import styled from 'styled-components';

export const Wrapper = styled.div`
  
  padding: 50px;
  height: 100% !important;
  
  
  img{
    object-fit: cover;
  }
`;
