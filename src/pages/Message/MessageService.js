import axios from 'axios';
import { Api } from '../../_helpers/service';

export const MessageApi = {
  getUser: id => Api.get(`api/user/${id}`),
  loadCheckMessage: userId => Api.get(`api/check/messages/${userId}`),
  loadChat: (userId, senderId) => Api.get(`api/check/messages/${userId}/${senderId}`),
  sendMessage: values => Api.post(`api/send/message`, values),
  setIsRead: values => Api.put(`api/change/message/read`, values),

  getUsers: params => {
    let data = new FormData();
    const json = JSON.stringify({});
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('filter', blob);
    return axios.post('api/search/users', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
