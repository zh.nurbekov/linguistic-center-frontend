import React, { useEffect, useState } from 'react';
import { Api } from '../../_helpers/service';
import Table from '../../_ui/Table/Table';
import { statementColumns } from './StatementColumns';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import { Typography, useTheme } from '@material-ui/core';
import FilingApplication from '../FilingApplications/FilingApplication';
import Modal from '../../_ui/Modal/Modal';

export const journalFilterName = '_journal';

export default function Statement() {
  const [statements, setStatements] = useState([]);
  const [count, setCount] = useState([]);
  const modal = useSimpleModal();
  const theme = useTheme();

  const onLoad = () => {
    Api.get('api/get/statements')
      .then(({ data }) => {
        setCount(data.count);
        setStatements(data.data);
      })
      .catch(e => console.log(e));
  };

  console.log(statements);

  return (
    <div className="px4 py2" style={{ height: '100%' }}>
      <div className="mb2">
        <Typography variant="h6" children={'Заявки'} />
      </div>
      <Table
        queryFilterName={journalFilterName}
        columns={statementColumns(modal.open)}
        data={statements}
        loading={false}
        total={count}
        loadData={filter => onLoad(filter)}
      />

      {modal.isOpen && (
        <Modal fullScreen title={''} onClose={() => modal.close()}>
          <div style={{ background: '#e4e4e4' }}>
            <FilingApplication  data={modal.data} />
          </div>
        </Modal>
      )}
    </div>
  );
}
