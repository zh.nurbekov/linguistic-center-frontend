import React from 'react';
import IconButton from '../../_ui/Button/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import Button from '../../_ui/Button/Button';
import moment from 'moment';

export const statementColumns = modalOnOpen => {
  const columns = [
    {
      Header: 'Дата',
      accessor: 'date',
      width: 200,
      Cell: ({ original: { date } }) => (
        <span className="flex justify-center col-12">{moment(date).format('DD.MM.YYYY')}</span>
      )
    },
    {
      Header: 'ФИО',
      accessor: 'date',
      Cell: ({ original }) => (
        <div className="flex col-12justify-center">
          {`${original.firstName} ${original.middleName}`}
        </div>
      )
    },
    {
      Header: 'Почта',
      accessor: 'email'
    },
    {
      Header: 'Пол',
      accessor: 'gender'
    },
    {
      Header: 'Дата рождений',
      accessor: 'birthDate'
    },
    {
      Header: '',
      accessor: 'date',
      Cell: ({ original }) => (
        <div className="flex col-12justify-center">
          <Button
            text={'Посмотреть'}
            variant={'text'}
            onClick={() => modalOnOpen(original)}
          />
        </div>
      )
    }
  ];

  return columns;
};
