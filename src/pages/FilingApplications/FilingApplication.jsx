import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, useState } from 'react';
import useSimpleModal from '../../components/_hooks/useSimpleModal';

import {
  DialogActions,
  DialogContent,
  Divider,
  Paper,
  Typography,
  useTheme
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Row from '../../_ui/Row';
import Input from '../../_ui/Input/Input';
import Form from '../../_ui/Form/Form';
import { Male } from '../../_helpers/constants';
import Select from '../../_ui/Select/Select';
import SendIcon from '@material-ui/icons/DoubleArrow';
import { Api } from '../../_helpers/service';
import Checkbox from '../../_ui/Checkbox/Checkbox';
import statusMessage from '../../utils/statusMessage';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/history';
import * as yup from 'yup';
import { loadCategory, PassTestModule } from '../PassTest/PassTestDucks';

const UsersValidate = yup.object().shape({
  firstName: yup.string().required('Обязательное поле для заполнения'),
  middleName: yup.string().required('Обязательное поле для заполнения'),
  birthDate: yup.string().required('Обязательное поле для заполнения'),
  email: yup.string().required('Обязательное поле для заполнения'),
  phone: yup
    .string()
    .required('Обязательное поле для заполнения')
    .test('length', 'Номер должен состоять менее 11 цифр', num => num && num.length < 12)
});

export default function FilingApplication({ data }) {
  const [currentCategory, setCurrentCategory] = useState(
    data ? parseInt(data.service) : null
  );
  const { category } = useSelector(state => state[PassTestModule]);
  const [isChild, setIsChild] = useState(
    data && data.hasOwnProperty('parent') ? data.parent : false
  );
  const { palette } = useTheme();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadCategory());
  }, []);

  const onSave = values => {
    Api.post('api/create/statement', {
      ...values,
      parent: isChild,
      service: currentCategory
    })
      .then(({ data }) => {
        const finish = () => {
          history.push(paths.homePage);
        };
        statusMessage(data, finish);
      })
      .catch(e => console.log(e));
  };

  console.log(currentCategory);
  return (
    <div className="p3" style={{ height: '100%' }}>
      {!data && (
        <div className="mb2">
          <Typography variant="h6" children={'Подача заявки'} />
        </div>
      )}
      <Paper>
        <div className="px4 py2">
          <Form validate={UsersValidate} initialValues={data} onSubmit={onSave}>
            <DialogContent className={'pt1'}>
              <h4 style={{ marginBottom: 10 }} children={'Обучающиеся'} />
              <Divider style={{ marginBottom: 10 }} />
              <div className="flex col-12">
                <div className="md-col-6 p1">
                  <Row label="Фамилия">
                    <Input fullWidth name="middleName" />
                  </Row>
                  <Row label="Имя">
                    <Input fullWidth name="firstName" />
                  </Row>
                  <Row label="Отчество">
                    <Input fullWidth name="lastName" />
                  </Row>
                  <Row label="Дата рождения">
                    <Input fullWidth name="birthDate" />
                  </Row>
                </div>
                <div className="md-col-6 p1">
                  <Row label={'Почта'}>
                    <Input fullWidth name={'email'} type={'email'} />
                  </Row>
                  <Row label={'Пол'}>
                    <Select
                      fullWidth
                      //label={'Пол'}
                      //variant={'outlined'}
                      options={Male}
                      name={'gender'}
                    />
                  </Row>
                  <Row label={'Контактные данные'}>
                    <Input fullWidth maxLength={11} name={'phone'} type={'number'} />
                  </Row>
                  <Row label={'Услуга'}>
                    {/*<Input fullWidth name={'service'} />*/}
                    <div>
                      <Select
                        withoutForm
                        name={'service'}
                        placeholder={'Выберите категорию'}
                        onChange={({ target: { value } }) => setCurrentCategory(value)}
                        value={currentCategory}
                        style={{ width: 300 }}
                        options={category.map(({ id, name }) => ({ value: id, name }))}
                      />
                    </div>
                  </Row>
                </div>
              </div>

              <Divider style={{ marginBottom: 10 }} />

              <div className="p1">
                <Checkbox
                  disabled={data}
                  onChange={({ target: { checked } }) => setIsChild(checked)}
                  withoutForm
                  label={'Если возраст обучаюшего меньше 13 лет'}
                  checked={isChild}
                />
              </div>
              <Divider style={{ marginBottom: 10 }} />

              {isChild && (
                <div className="mt2">
                  <h4
                    style={{ marginBottom: 10, marginTop: 20 }}
                    children={'Законный представитель'}
                  />
                  <Divider style={{ marginBottom: 10 }} />
                  <div className="flex col-12">
                    <div className="md-col-6 p1">
                      <Row label="Фамилия" s>
                        <Input fullWidth name="parentMiddleName" />
                      </Row>
                      <Row label="Имя">
                        <Input fullWidth name="parentFirstName" />
                      </Row>
                      <Row label="Отчество">
                        <Input fullWidth name="parentLastName" />
                      </Row>
                    </div>
                    <div className="md-col-6 p1">
                      <Row label="Дата рождения">
                        <Input fullWidth name="parentBirthDate" />
                      </Row>
                      <Row label={'Почта'}>
                        <Input fullWidth name={'parentEmail'} />
                      </Row>
                      <Row label={'Пол'}>
                        <Select fullWidth options={Male} name={'parentGender'} />
                      </Row>
                    </div>
                  </div>
                </div>
              )}
            </DialogContent>
            {!data && (
              <DialogActions>
                <Button
                  color={'primary'}
                  type="submit"
                  variant={'contained'}
                  children={'Отправить'}
                >
                  Отправить
                  <SendIcon style={{ marginLeft: 10 }} />
                </Button>
              </DialogActions>
            )}
          </Form>
        </div>
      </Paper>
    </div>
  );
}
