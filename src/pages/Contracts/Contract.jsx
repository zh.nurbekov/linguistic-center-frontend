import React, { useEffect, useState } from 'react';
import { Api } from '../../_helpers/service';
import { localSecureStorage } from '../../_app/App';
import Button from '../../_ui/Button/Button';
import GetAppIcon from '@material-ui/icons/GetApp';


export default function Contracts() {
  const [contract, setContract] = useState();
  const user = localSecureStorage.getItem('user');

  useEffect(() => {
    Api.get(`api/genered/report/contract/${user.id}`)
      .then(({ data }) => console.log(data))
      .catch(e => console.log(e));
  }, []);

  return <>
  <div style={{height:200}} className='flex col-12 justify-center items-center'>

    <Button text={'Загрузить договор'} icon={<GetAppIcon/>} />

  </div>
  </>;
}
