import getOptions from '../../utils/getOptions';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { groupModule, loadGroups } from '../Groups/GroupDucks';
import Select from '../../_ui/Select/Select';
import { Paper, Typography } from '@material-ui/core';
import { Api } from '../../_helpers/service';
import Button from '../../_ui/Button/Button';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import CreateHomeWork from './CreateHomeWork';
import Access from '../../components/access/Access';
import { PERMISSIONS } from '../../_helpers/permissions';

export default function HomeWork() {
  const { groups } = useSelector(state => state[groupModule]);
  const [work, setWork] = useState();
  const [group, setGroup] = useState();
  const modal = useSimpleModal();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadGroups());
  }, []);

  console.log(work);

  useEffect(() => {
    if (group) {
      Api.get(`api/get/homework/${group}`)
        .then(({ data: { data } }) => (data.length ? setWork(data[0]) : setWork(null)))
        .catch(e => console.log(e));
    }
  }, [group]);

  return (
    <div className="px4 py2" style={{ height: '80vh' }}>
      <div className="flex justify-between my1">
        <div className="mb2">
          <Typography variant="h6" children={'Домашнее задание'} />
        </div>
      </div>
      <Paper>
        <div className="flex justify-between py1">
          <div style={{ width: 200 }} className="px1">
            <Select
              variant={'outlined'}
              withoutForm
              placeholder={'Группы'}
              name={'groupId'}
              value={group}
              onChange={({ target: { value } }) => setGroup(value)}
              options={getOptions(groups)}
            />
          </div>
          <Access permissions={PERMISSIONS.HOME_WORK_CREATE}>
            <Button
              disabled={!group}
              text={'Создать'}
              style={{ marginRight: 20 }}
              onClick={() => modal.open(group)}
            />
          </Access>
        </div>
        <div className="col-12 center" style={{ height: 200 }}>
          {work && <Typography variant={'body2'} children={work.textHomework} />}
          {work && work.document && <a href={work.document}>Скачать документ</a>}
          {!work && <Typography variant={'body2'} children={'Заданий пока нет'} />}
        </div>
      </Paper>
      {modal.isOpen && <CreateHomeWork {...modal} />}
    </div>
  );
}
