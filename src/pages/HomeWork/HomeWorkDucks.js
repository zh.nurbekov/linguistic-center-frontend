import { createReducer } from '@reduxjs/toolkit';
import { HomeWorkApi } from './HomeWorkService';

/**
 * Constants
 */

export const HomeWorkModule = 'HomeWork';
export const WORKS = `${HomeWorkModule}/WORKS`;
export const CREATE = `${HomeWorkModule}/CREATE`;
export const UPDATE = `${HomeWorkModule}/UPDATE`;
export const LOADING = `${HomeWorkModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  works: [],
  count: 0,
  loading: false
};

export default createReducer(initialState, {
  [WORKS]: (state, { payload: { data, count } }) => {
    state.works = data;
    state.count = count;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.works = [...state.works, payload];
    state.count = ++state.count;
  },

  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadWork = group => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await HomeWorkApi.loadWorks(group);
    dispatch({ type: WORKS, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const createWork = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await HomeWorkApi.createWork(filter);
    dispatch({ type: CREATE, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

// export const createJournal = (values, close) => async dispatch => {
//   try {
//     let { data } = await HomeWorkApi.createJournal(values);
//     dispatch({ type: CREATE, payload: data });
//     close && close();
//   } catch (e) {
//     console.error(e);
//   }
// };
//
// export const updateJournal = (values, close) => async dispatch => {
//   try {
//     let { data } = await HomeWorkApi.updateJournal(values);
//     dispatch({ type: CREATE, payload: data });
//     close && close();
//   } catch (e) {
//     console.error(e);
//   }
// };
