import { Api } from '../../_helpers/service';
import axios from 'axios';

export const HomeWorkApi = {
  loadWorks: (group) => Api.get(`api/get/homework/${group}`),
  createWork: (params, document) => {
    let data = new FormData();
    const json = JSON.stringify(params);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('homeworkData', blob);
    data.append('document', document);
    return axios.post('api/create/homework', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
