import React from 'react';
import IconButton from '../../_ui/Button/IconButton';
import EditIcon from '@material-ui/icons/Edit';

export const workColumns = modalOnOpen => {
  const columns = [
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      Cell: ({ original: { rowNum } }) => (
        <span className="flex justify-center col-12">{rowNum}</span>
      )
    },
    {
      Header: 'Дата',
      accessor: 'date',
      width: 200
    },
    {
      Header: 'Группа',
      accessor: 'groupName'
    },
    {
      Header: 'Группа',
      accessor: 'groupName',
      width: 100,
      Cell: ({ original: { date, groupId } }) => (
        <div className="flex col-12justify-center">
          <IconButton
            icon={<EditIcon />}
            onClick={() => modalOnOpen({ date, groupId })}
          />
        </div>
      )
    }
  ];

  return columns;
};
