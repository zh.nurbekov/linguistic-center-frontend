import React, { useState } from 'react';
import Modal from '../../_ui/Modal/Modal';
import Input from '../../_ui/Input/Input';
import { DialogActions, DialogContent } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { useTheme } from '@material-ui/core/styles';
import { HomeWorkApi } from './HomeWorkService';
import { localSecureStorage } from '../../_app/App';

export default function CreateHomeWork({ close, data: groupId }) {
  const { palette } = useTheme();
  const [textHomework, setTextHomework] = useState();
  const [document, setDocument] = useState();
  const user = localSecureStorage.getItem('user');

  const onCreate = () => {
    HomeWorkApi.createWork(
      { textHomework, teacherId: user.id, groupId },
      document
    ).then();
  };

  return (
    <Modal maxWidth={'md'} title={'Создать задание'} onClose={() => close()}>
      <DialogContent dividers className={'pt1'}>
        <div>
          <div className="px3 my2">
            <Input
              withoutForm
              multiline
              rows={3}
              fullWidth
              value={textHomework}
              onChange={({ target: { value } }) => setTextHomework(value)}
              label="Дополнительная информация"
              name={'information'}
            />
          </div>
          <div className="flex justify-end">
            <input
              name="myFile"
              type="file"
              onChange={({ target: { files } }) => setDocument(files[0])}
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={onCreate}
          type="submit"
          variant={'contained'}
          children={'Создать'}
          style={{ color: 'white', background: palette.success.main }}
        />
      </DialogActions>
    </Modal>
  );
}
