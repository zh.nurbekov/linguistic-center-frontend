import React from 'react';
import { Card, Typography } from '@material-ui/core';

export default function Contacts() {
  return (
    <div className="col-12  " style={{ height: '55vh' }}>
      <div className="flex justify-center " style={{ marginTop: 200 }}>
        <div className="col-2" />
        <div className=" p4 col-8">
          <div style={{ width: '100%', textAlign: 'center' }}>
            <Typography variant="h5" children={'Контактные данные'} />
            <br />
            <Typography
              variant="body1"
              children={' ул. Глушко 2, 123 кабинет (первый этаж)'}
            />
            <Typography variant="body1" children={' +7 (771) 524-66-89'} />
          </div>
        </div>
        <div className="col-2" />
      </div>
      <div className="flex justify-center ">
        <a href="https://vk.com/stepupbaykonur" target="_blank" className='mr2'>
          <img
            width={50}
            src={
              'https://avatanplus.ru/files/resources/original/5817612165d991581b536a89.png'
            }
          />
        </a>
        <a href="https://www.instagram.com/stepup__baik/" target="_blank">
          <img
            width={50}
            src={
              'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/2048px-Instagram_icon.png'
            }
          />
        </a>
      </div>
    </div>
  );
}
