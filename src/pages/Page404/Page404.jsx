import React from 'react';
import { CssBox } from './Page404Style';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import paths from '../../_helpers/paths';

export default function Page404() {
  return (
    <CssBox>
      <div className="container">
        <div className="error">404</div>
        <div className="oops">{'Упс, Страница не найдена!'}</div>
        <div className="not-found">
          {'Возможно вы ввели не правильный адрес, \n или страница была удалена.'}
        </div>
        <Link to={paths.homePage}>
          <Button variant="contained" color="secondary" children={'на главную'} />
        </Link>
      </div>
    </CssBox>
  );
}
