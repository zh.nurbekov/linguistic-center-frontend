import React from 'react';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  makeStyles,
  Typography
} from '@material-ui/core';
import { Wrapper } from '../HomePage/HomePageStyles';

const useStyles = makeStyles({
  root: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

export default function Service() {
  const classes = useStyles();
  return (
    <Wrapper>
      <div
        className="col-12   flex justify-center mt2"
        style={{ height: '65vh', paddingBottom: 200 }}
      >
        <div style={{ width: 750 }}>
          <div className=" flex " style={{marginBottom:100}}>
            <div style={{ width: 300, marginRight: 30 }}>
              <Card className={classes.root} title={'Подготовка к экзаменам'}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="https://master-prod.s3.eu-central-1.amazonaws.com/organization/78688organization.jpg"
                  />
                  <CardContent>
                    <h6>Подготовка к экзаменам</h6>
                    <Typography gutterBottom variant="body2">
                      Если вы хотите подготовиться к сдаче экзамена по английскому и
                      отрабатывать типовые задания, то Вам нужно к нам!
                    </Typography>
                    <Typography variant="h6" color="textSecondary" component="p">
                      Цена – 3 200 рублей
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </div>
            <div style={{ width: 300 }}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="https://dhsh-durtuli.bash.muzkult.ru/media/2019/09/16/1262749344/banners.jpg"
                    title="Contemplative Reptile"
                  />
                  <CardContent>
                    <h6>Занятия с дошкольниками</h6>
                    <Typography gutterBottom variant="body2">
                      Занятия с самыми маленькими это очень интересно и познавательно!
                      Уроки проводятся в будние и выходные дни 2 раза в неделю.
                    </Typography>
                    <Typography variant="h6" color="textSecondary" component="p">
                      Цена – 2 000 рублей
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </div>
          </div>
          <div className=" mt2 flex  ">
            <div style={{ width: 300, marginRight: 30 }}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="https://cdn4.mygazeta.com/i/2020/03/01.jpg"
                    title="Contemplative Reptile"
                  />
                  <CardContent>
                    <h6>Занятия со школьниками</h6>
                    <Typography gutterBottom variant="body2">
                      Уроки для школьников проводятся либо для того чтобы "догнать"
                      школьную программу либо чтобы практиковать разговорную речь! Занятия
                      идут в будние дни в удобное для школьника время по 60 минут 2 раза в
                      неделю!
                    </Typography>
                    <Typography variant="h6" color="textSecondary" component="p">
                      Цена – 2 800 рублей
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </div>
            <div style={{ width: 300 }}>
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image="https://24.kz/media/k2/items/cache/4822ede6b257fbd1625bd8a8eb04e291_XL.jpg"
                    title="Contemplative Reptile"
                  />
                  <CardContent>
                    <h6>Занятия со взрослыми </h6>
                    <Typography gutterBottom variant="body2">
                      Занятия проводятся в будние вечера 2 раза в неделю по 60 минут!
                    </Typography>
                    <Typography variant="h6" color="textSecondary" component="p">
                      Цена – 2 800 рублей
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  );
}
