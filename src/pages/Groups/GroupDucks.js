import { createReducer } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import { GroupsApi } from './GroupService';
import statusMessage from '../../utils/statusMessage';

/**
 * Constants
 */

export const groupModule = 'group';
export const GROUPS = `${groupModule}/GROUPS`;
export const CREATE = `${groupModule}/CREATE`;
export const DELETE = `${groupModule}/DELETE`;
export const LOADING = `${groupModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  groups: [],
  groupsCount: 0,
  loading: false
};

export default createReducer(initialState, {
  [GROUPS]: (state, { payload: { data, count } }) => {
    state.groups = data;
    state.groupsCount = count;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    const groups = state.groups.some(({ id }) => id === payload.id)
      ? [...state.groups.map(item => (item.id === payload.id ? payload : item))]
      : [...state.groups, payload];
    state.groups = [...groups];
    state.groupsCount = !state.groups.some(({ id }) => id === payload.id)
      ? state.groupsCount
      : state.groupsCount + 1;
  },
  [DELETE]: (state, { payload: { groupId, userId } }) => {
    console.log(groupId, userId);
    state.groups = [
      ...state.groups.map(item =>
        item.id === groupId
          ? { ...item, users: item.users.filter(({ id }) => id !== userId) }
          : item
      )
    ];
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadGroups = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await GroupsApi.loadGroups(filter);
    console.log(data);
    dispatch({ type: GROUPS, payload: { data, count: data.length } });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const createGroups = (group, users, isCreate, close) => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let newData = {};
    let params = {};
    if (isCreate) {
      let { data } = await GroupsApi.createGroup(group);
      params = { usersId: users.map(({ id }) => id), groupId: data.id };
      newData = { ...data, users };
    } else {
      params = { usersId: users.map(({ id }) => id), groupId: group.id };
      newData = { ...group, users };
    }
    let result = await GroupsApi.addUserGroup(params);
    const update = () => {
      console.log(newData);
      dispatch({
        type: CREATE,
        payload: newData
      });
    };
    statusMessage(result.data, update);
    dispatch({ type: LOADING, loading: false });
    close && close();
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const deleteUserInGroups = (groupId, userId) => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await GroupsApi.deleteUserInGroups(userId);
    console.log(data);
    dispatch({ type: DELETE, payload: { groupId, userId } });
    dispatch({ type: LOADING, loading: false });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};
