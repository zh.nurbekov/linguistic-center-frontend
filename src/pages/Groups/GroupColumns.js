import React from 'react';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { IconButton } from '@material-ui/core';
import MuiIconButton from '@material-ui/core/IconButton';
import DiscloseIcon from '@material-ui/icons/ArrowRight';
import CloseIcon from '@material-ui/icons/ArrowDropDown';
import Access from '../../components/access/Access';
import { PERMISSIONS } from '../../_helpers/permissions';

export const groupColumns = (modalIsOpen, editExpanded) => {
  const Expander = ({ original, isExpanded, index, editExpanded }) => {
    if (original['users']) {
      return (
        <MuiIconButton
          size="small"
          children={isExpanded ? <CloseIcon /> : <DiscloseIcon />}
          onClick={e => {
            e.stopPropagation();
            editExpanded(isExpanded, index, original);
          }}
        />
      );
    }
    return null;
  };

  const columns = [
    {
      Header: '',
      expander: true,
      width: 50,
      Expander: eProps => (
        <div className="flex items-center">
          <div style={{ width: 32, height: 30 }}>
            <Expander {...eProps} editExpanded={editExpanded} />
          </div>
        </div>
      )
    },
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      Cell: ({ original: { rowNum } }) => (
        <div className="flex justify-center col-12">{rowNum}</div>
      )
    },
    {
      Header: 'Имя',
      accessor: 'name'
    },
    {
      Header: `Количество учеников `,
      width: 250,
      accessor: 'action',
      Cell: ({ original }) => (
        <div className="col-12 flex justify-center items-center">
          {original.users.length}
        </div>
      )
    },
    {
      Header: '',
      width: 150,
      accessor: 'action',
      Cell: ({ original }) => (
        <div className="col-12 flex justify-center">
          <Access permissions={PERMISSIONS.GROUPS_EDIT}>
            <IconButton onClick={() => modalIsOpen(original)} style={{ padding: 5 }}>
              <PersonAddIcon />
            </IconButton>
          </Access>
        </div>
      )
    }
  ];

  return columns;
};
