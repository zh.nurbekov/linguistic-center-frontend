import React from 'react';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { confirm } from '../../utils/utils';
import hasAccess from '../../components/access/hasAccess';
import { PERMISSIONS } from '../../_helpers/permissions';
import Access from '../../components/access/Access';

export const userSubColumns = deleteUserInGroup => {
  const columns = [
    {
      Header: 'Имя',
      accessor: 'firstname',
      Cell: ({ original: { firstname } }) => <div className="pl2">{firstname}</div>
    },
    { Header: 'Фамилия', accessor: 'middlename', filterable: false },
    { Header: 'Контакты', accessor: 'phone', filterable: false },
    { Header: 'Дата рождений', accessor: 'birthdate', filterable: false },
    {
      Header: 'Тип пользователя',
      accessor: 'userType',
      Cell: ({ original }) => (
        <div className="col-12 flex justify-center">
          <Access permissions={PERMISSIONS.GROUPS_EDIT}>
            <IconButton
              onClick={() => {
                confirm('Вы действительно хотите удалить пользователя из  группы ?', () =>
                  deleteUserInGroup(original.id)
                );
              }}
              style={{ padding: 5 }}
            >
              <DeleteIcon />
            </IconButton>
          </Access>
        </div>
      )
    }
  ];

  return columns;
};
