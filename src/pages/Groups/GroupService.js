import axios from 'axios';
import { Api } from '../../_helpers/service';

export const GroupsApi = {
  loadGroups: () => Api.get('api/groups'),
  createGroup: values => Api.post('api/create/group/level', values),
  addUserGroup: values => Api.put('api/add/group/users', values),
  deleteUserInGroups: id => Api.delete(`api/delete/user/from/group/${id}`),
  updateUser: user => Api.put(`/user/${user.id}`, user),
  createUser: user => Api.post('api/registr', user),

};
