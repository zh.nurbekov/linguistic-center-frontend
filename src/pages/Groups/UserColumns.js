import React from 'react';
import { Checkbox } from '@material-ui/core';

export const userColumns = (userTypes, category, onChecked, checkedUsers) => {
  const columns = [
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      Cell: ({ original }) => {
        return (
          <Checkbox
            checked={checkedUsers.some(({ id }) => id === original.id)}
            onChange={({ target: { checked } }) => onChecked(original, checked)}
          />
        );
      }
    },
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      filterable: false
    },
    {
      Header: 'Имя',
      accessor: 'firstname'
    },
    { Header: 'Фамилия', accessor: 'middlename', filterable: false },
    { Header: 'Контакты', accessor: 'phone', filterable: false },
    { Header: 'Дата рождения', accessor: 'birthdate', filterable: false },
    {
      Header: `Результаты тестирования`,
      width: 200,
      accessor: 'action',
      Cell: ({ original }) => (
        <div className="col-12 flex justify-center items-center">
          {original.resultTest}
        </div>
      )
    },
    {
      Header: 'Тип пользователя',
      accessor: 'userType',
      Cell: ({ original: { userType } }) => {
        const type = userTypes.find(({ id }) => id === userType);
        return <div>{type ? type.name : ''}</div>;
      }
    }
  ];

  return columns;
};
