import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

let sortOrder = 'asc';
function useTableExpander(index, name) {
  const filterName = `filter${index}`;
  const { search } = useLocation();
  const params = new URLSearchParams(search);
  const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : {};
  const [expanded, setExpanded] = useState({});

  useEffect(() => {
    sortOrder = filter.sort_order;
    setExpanded({});
  }, [search]);

  const editExpanded = (isExpanded, index) => {
    const newExpanded = { ...expanded };
    isExpanded ? delete newExpanded[index] : (newExpanded[index] = true);
    setExpanded(newExpanded);
  };

  return { expanded, editExpanded };
}

export default useTableExpander;
