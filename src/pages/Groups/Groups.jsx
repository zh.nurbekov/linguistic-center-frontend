import React, { useEffect } from 'react';
import { deleteUserInGroups, groupModule, loadGroups } from './GroupDucks';
import { groupColumns } from './GroupColumns';
import { useDispatch, useSelector } from 'react-redux';
import Table from '../../_ui/Table/Table';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import GroupsCreateModal from './GroupsCreateModal';
import Button from '../../_ui/Button/Button';
import ReactTable from 'react-table';
import useTableExpander from './useTableExpander';
import { Wrapper } from './GroupStyles';
import { userSubColumns } from './userSubColumns';
import { Typography } from '@material-ui/core';
import Access from '../../components/access/Access';
import { PERMISSIONS } from '../../_helpers/permissions';

export const groupFilterName = '_group';
function Groups({}) {
  const { groups, groupsCount, loading } = useSelector(state => state[groupModule]);
  const createModal = useSimpleModal();
  const { expanded, editExpanded } = useTableExpander('');

  const dispatch = useDispatch();
  const deleteUserGroup = (groupId, userId) =>
    dispatch(deleteUserInGroups(groupId, userId));
  useEffect(() => {}, []);

  return (
    <div className="px4 py2" style={{ height: '100%' }}>
      <div className="flex justify-between my2">
        <div className="mb2">
          <Typography variant="h6" children={'Группы'} />
        </div>
        <Access permissions={PERMISSIONS.GROUPS_EDIT}>
          <Button text={'Создать группу'} onClick={() => createModal.open({})} />
        </Access>
      </div>
      <Table
        expanded={expanded}
        queryFilterName={groupFilterName}
        columns={groupColumns(createModal.open, editExpanded)}
        data={loading ? [] : groups}
        loading={false}
        total={groupsCount}
        loadData={filter => dispatch(loadGroups(filter))}
        SubComponent={({ original }) => {
          return (
            <Wrapper>
              <div className="p2">
                {original.users.length ? (
                  <ReactTable
                    TheadComponent={() => null}
                    style={{ border: 'none' }}
                    data={original.users}
                    columns={userSubColumns(userId =>
                      deleteUserGroup(original.id, userId)
                    )}
                    defaultPageSize={original.users.length}
                    showPagination={false}
                  />
                ) : (
                  <div
                    className="flex justify-center items-center"
                    style={{ height: 20 }}
                  >
                    Список пуст
                  </div>
                )}
              </div>
            </Wrapper>
          );
        }}
      />
      {createModal.isOpen && <GroupsCreateModal {...createModal} />}
    </div>
  );
}

export default Groups;
