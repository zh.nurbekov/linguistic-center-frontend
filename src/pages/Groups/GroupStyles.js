import styled from 'styled-components';

export const Wrapper = styled.div`
  background-color: rgba(234, 234, 234, 0.55);

  .rt-th.rt-resizable-header {
    height: 30px;
    display: flex;
    justify-content: left;
    border: none;
    border-radius: 0;
  }

  .ReactTable  {
    background: white;
  }
  
  .ReactTable .rt-thead .rt-tr {
    text-align: left;
  }

  .rt-table {
    margin-top: 3px;
    background-color: rgba(255, 255, 255, 0.71);
    border: none !important;
  }

  .rt-thead.-header {
    border: 1px solid rgba(0, 0, 0, 0.05);
  }

  .ReactTable .rt-table {
    border-radius: 0;
  }

  .ReactTable .rt-tbody {
    background-color: white;
  }

  .rt-resizable-header-content {
    width: 100%;
  }
`;
