import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, DialogActions, DialogContent, useTheme } from '@material-ui/core';
import Modal from '../../_ui/Modal/Modal';
import Form from '../../_ui/Form/Form';
import Input from '../../_ui/Input/Input';
import Table from '../../_ui/Table/Table';
import { userFilterName } from '../UserManagePage/Users/Users';
import { userColumns } from './UserColumns';
import { STUDENT_TYPE } from '../../_helpers/constants';
import { createGroups } from './GroupDucks';
import * as yup from 'yup';
import {
  loadCategory,
  loadRoles,
  loadUsers,
  loadUserType,
  usersModule
} from '../UserManagePage/Users/UsersDucks';
import Select from '../../_ui/Select/Select';
import usePushNewPageWithFilter from '../../components/Filters/usePushNewPageWithFilter';

export const GroupsValidate = yup.object().shape({
  name: yup.string().required('Обязательное поле для заполнения')
});

function GroupsCreateModal({ data, close, dataIsEmpty }) {
  const dispatch = useDispatch();
  const { palette } = useTheme();
  const { push, queryFilter } = usePushNewPageWithFilter(userFilterName);
  const { users, usersCount, category, userType } = useSelector(
    state => state[usersModule]
  );

  const [checkedUsers, setCheckedUsers] = useState(!dataIsEmpty ? data.users : []);

  const onChecked = item => {
    const newList = checkedUsers.some(({ id }) => item.id === id)
      ? checkedUsers.filter(({ id }) => item.id !== id)
      : [...checkedUsers, item];
    setCheckedUsers(newList);
  };

  useEffect(() => {
    dispatch(loadRoles());
    dispatch(loadUserType());
  }, []);

  useEffect(() => {
    dispatch(loadCategory());
  }, []);

  const onSubmit = values => {
    const groupData = !dataIsEmpty ? data : values;
    dispatch(createGroups(groupData, checkedUsers, dataIsEmpty,close));
  };

  return (
    <div>
      <Modal maxWidth={'lg'} title={'Добавление пользователя'} onClose={() => close()}>
        <Form
          validate={GroupsValidate}
          initialValues={data}
          onSubmit={onSubmit}
          render={({ userType: type }) => (
            <>
              <DialogContent dividers className={'pt1'}>
                <div className="my2 flex items-center">
                  <Input
                    style={{marginTop:9}}
                    name={'name'}
                    label={'Название группы'}
                    disabled={!dataIsEmpty}
                  />
                  <Select
                    withoutForm
                    value={queryFilter.category}
                    placeholder={'Выберите категорию'}
                    onChange={({ target: { value } }) => push({ category: value })}
                    style={{ width: 300, marginLeft: 20 }}
                    options={category.map(({ id, name }) => ({ value: id, name }))}
                  />
                </div>
                <Table
                  queryDefaultParam={{ userType: STUDENT_TYPE }}
                  queryFilterName={userFilterName}
                  columns={userColumns(userType, category, onChecked, checkedUsers)}
                  data={
                    dataIsEmpty
                      ? users
                      : users.filter(({ id }) => !data.users.some(usr => usr.id === id))
                  }
                  loading={false}
                  total={usersCount}
                  loadData={filter =>
                    dispatch(loadUsers({ ...filter, addedToGroup: false }))
                  }
                />
              </DialogContent>
              <DialogActions>
                <Button
                  style={{ color: 'white', background: palette.error.main }}
                  children="Отмена"
                  variant={'contained'}
                  onClick={close}
                />
                <Button
                  type="submit"
                  variant={'contained'}
                  children={'Сохранить'}
                  style={{ color: 'white', background: palette.success.main }}
                />
              </DialogActions>
            </>
          )}
        />
      </Modal>
    </div>
  );
}

export default GroupsCreateModal;
