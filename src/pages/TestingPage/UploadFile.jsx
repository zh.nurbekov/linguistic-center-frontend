import React from 'react';
import Button from '../../_ui/Button/Button';

export default function UploadFile({ onChange }) {
  const handleClick = () => {
    document.getElementById('file-input').click();
  };

  const fileUpload = ({ target: { files } }) => {
    onChange(files[0]);
  };

  return (
    <>
      <Button text={'Выберите файл'} variant={'contained'} onClick={handleClick} />
      <input
        id="file-input"
        type="file"
        style={{ display: 'none' }}
        onChange={fileUpload}
      />
    </>
  );
}
