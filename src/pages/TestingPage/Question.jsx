import React from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  useTheme
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import IconButton from '../../_ui/Button/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { deleteQuestions } from './TastingDucks';
import { useDispatch } from 'react-redux';
import { confirm } from '../../utils/utils';

export default function Question({ question }) {
  const { palette } = useTheme();
  const dispatch = useDispatch();
  return (
    <div className="mt2">
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <div className="flex col-12 justify-between items-center">
            <Typography>{question.name}</Typography>
            <IconButton
              icon={
                <DeleteIcon
                  onClick={e => {
                    e.stopPropagation();
                    confirm('Вы уверенны что хотите удалить данный вопрос ? ', () =>
                      dispatch(deleteQuestions(question.id))
                    );
                  }}
                />
              }
            />
          </div>
        </AccordionSummary>
        <AccordionDetails>
          <div className="mb2">
            {console.log(question.image)}
            {question.image && question.image.indexOf('null') < 0 && (
              <img src={question.image} width={300} />
            )}
            {question.questionValue.map((item, index) => {
              return (
                <Typography
                  children={`${index + 1} )   ${item.name}`}
                  style={{
                    color: item.correct ? palette.success.main : palette.text.primary
                  }}
                />
              );
            })}
          </div>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
