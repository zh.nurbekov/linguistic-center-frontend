import { Paper, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadCategory, loadQuestions, testingModule } from './TastingDucks';
import Select from '../../_ui/Select/Select';
import Button from '../../_ui/Button/Button';
import Question from './Question';
import QuestionsEditModal from './QuestionsEditModal';
import useSimpleModal from '../../components/_hooks/useSimpleModal';

export default function Testing() {
  const { category, questions } = useSelector(state => state[testingModule]);
  const [currentCategory, setCurrentCategory] = useState(1);
  const dispatch = useDispatch();
  const modal = useSimpleModal();

  useEffect(() => {
    dispatch(loadCategory());
  }, []);

  useEffect(() => {
    dispatch(loadQuestions());
  }, []);

  return (
    <div className="p3" style={{ height: '100%' }}>
      <div className="mb2">
        <Typography variant="h6" children={'Tестирование'} />
      </div>
      <Paper>
        <div className="px4 py2 flex justify-between">
          <Select
            onChange={({ target: { value } }) => setCurrentCategory(value)}
            value={currentCategory}
            style={{ width: 200 }}
            variant={'outlined'}
            withoutForm
            options={category.map(({ id, name }) => ({ value: id, name }))}
          />
          <Button text={'Добавить вопрос'} onClick={() => modal.open({})} />
        </div>
      </Paper>
      {questions
        .filter(item => item.categoryGroup.id === currentCategory)
        .map(item => (
          <Question question={item} />
        ))}
      {modal.isOpen && <QuestionsEditModal {...modal} categoryGroup={currentCategory} />}
    </div>
  );
}
