import { Api } from '../../_helpers/service';
import axios from 'axios';

export const TestingApi = {
  loadQuestions: () => Api.get('api/questions'),
  loadCategory: () => Api.get('api/categories'),
  deleteQuestions: (id) => Api.delete(`api/delete/question/${id}`),
  createQuestions: (questionData, image) => {
    let data = new FormData();
    const json = JSON.stringify(questionData);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('questionData', blob);
    image && data.append('image', image);
    return axios.post('api/create/question', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
