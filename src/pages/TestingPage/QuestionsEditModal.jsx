import Modal from '../../_ui/Modal/Modal';
import React, { useState } from 'react';
import Form from '../../_ui/Form/Form';
import Input from '../../_ui/Input/Input';
import { Checkbox, DialogActions, DialogContent, useTheme } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Row from '../../_ui/Row';
import { createQuestions } from './TastingDucks';
import { useDispatch } from 'react-redux';
import UploadFile from './UploadFile';
import * as yup from 'yup';

export const Validate = yup.object().shape({
  name: yup.string().required('Обязательное поле для заполнения')
});

export default function QuestionsEditModal({ close, categoryGroup }) {
  const [questionValues, setQuestionValues] = useState([]);
  const [image, setImage] = useState(null);
  const { palette } = useTheme();
  const dispatch = useDispatch();

  const onChange = (params, number) => {
    const newList = [
      ...questionValues.map((item, index) =>
        index === number ? { ...item, ...params } : item
      )
    ];
    setQuestionValues(newList);
  };

  const onSubmit = values => {
    const newQuestions = { ...values, questionValues, categoryGroup };
    dispatch(createQuestions(newQuestions, image, close));
  };
  return (
    <>
      <Modal maxWidth={'lg'} title={'Добавление вопроса'} onClose={() => close()}>
        <Form validate={Validate} onSubmit={onSubmit}>
          <DialogContent dividers className={'pt1'}>
            <div>
              <Row label="Вопрос">
                <Input multiline fullWidth rows={3} name="name" variant={'outlined'} />
              </Row>
              <div className="flex items-center justify-end my3">
                <div className="mr1"> {image && image.name}</div>
                <UploadFile onChange={setImage} />
                <Button
                  className="ml1"
                  onClick={() => setQuestionValues([...questionValues, { name: '' }])}
                  color={'primary'}
                  variant={'contained'}
                  children="Добавить варианты"
                />
              </div>
              {questionValues.map((item, index) => (
                <div className="mb1">
                  <Row label={`Вариант ${index + 1}`}>
                    <div className="flex items-center">
                      <Input
                        withoutForm
                        value={questionValues[index.name]}
                        fullWidth
                        name="name"
                        onChange={({ target: { value } }) =>
                          onChange({ name: value }, index)
                        }
                      />
                      <Checkbox
                        style={{ marginLeft: 10 }}
                        checked={item.correct}
                        onChange={e => {
                          onChange({ correct: e.target.checked }, index);
                        }}
                      />
                    </div>
                  </Row>
                </div>
              ))}
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              style={{ color: 'white', background: palette.error.main }}
              children="Отмена"
              variant={'contained'}
              onClick={close}
            />
            <Button
              type="submit"
              variant={'contained'}
              children={'Создать'}
              style={{ color: 'white', background: palette.success.main }}
            />
          </DialogActions>
        </Form>
      </Modal>
    </>
  );
}
