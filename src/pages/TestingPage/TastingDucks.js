import { createReducer } from '@reduxjs/toolkit';
import { TestingApi } from './TestingService';
import { NotificationManager } from 'react-notifications';
import statusMessage from '../../utils/statusMessage';

/**
 * Constants
 */

export const testingModule = 'testing';
export const QUESTIONS = `${testingModule}/QUESTIONS`;
export const ADD_QUESTIONS = `${testingModule}/ADD_QUESTIONS`;
export const CATEGORY = `${testingModule}/CATEGORY`;
export const DELETE = `${testingModule}/DELETE`;
export const LOADING = `${testingModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  questions: [],
  category: [],
  loading: false
};

export default createReducer(initialState, {
  [QUESTIONS]: (state, { payload }) => {
    state.questions = payload;
  },
  [ADD_QUESTIONS]: (state, { payload }) => {
    state.questions = [...state.questions, payload];
  },
  [CATEGORY]: (state, { payload }) => {
    state.category = payload;
  },
  [DELETE]: (state, { payload }) => {
    state.questions = [...state.questions.filter(({ id }) => id !== payload)];
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadQuestions = () => async dispatch => {
  try {
    let { data } = await TestingApi.loadQuestions();
    dispatch({ type: QUESTIONS, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const loadCategory = () => async dispatch => {
  try {
    let { data } = await TestingApi.loadCategory();
    dispatch({ type: CATEGORY, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const deleteQuestions = id => async dispatch => {
  try {
    let { data } = await TestingApi.deleteQuestions(id);
    const success = () => {
      dispatch({ type: DELETE, payload: id });
    };
    statusMessage(data, success);
  } catch (e) {
    console.error(e);
  }
};

export const createQuestions = (questions, image, modelOnClose) => async dispatch => {
  try {
    let { data } = await TestingApi.createQuestions(questions, image);
    dispatch({ type: ADD_QUESTIONS, payload: data });
    NotificationManager.success('Данные успешно сохранены');
    modelOnClose && modelOnClose();
  } catch (e) {
    console.error(e);
  }
};
