import React from 'react';
import { CssBox } from './Page403Style';
import { Link } from 'react-router-dom';
import paths from '../../_helpers/paths';
import Button from '@material-ui/core/Button';

export default function Page403() {
  return (
    <CssBox>
      <div className="container">
        <div className="error">403</div>
        <div className="oops">{'Упс, Отказано в доступе!'}</div>
        <div className="not-found">
          {'Возможно вы ввели не правильный адрес, \n или страница была удалена.'}
        </div>
        <Link to={paths.homePage}>
          <Button
            variant="contained"
            color="secondary"
            children={'на главную'}
          />
        </Link>
      </div>
    </CssBox>
  );
}
