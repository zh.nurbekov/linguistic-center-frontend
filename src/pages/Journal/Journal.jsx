import React, { useEffect } from 'react';
import FilterSelect from '../../components/TableFilter/ColumnsFilters/FilterSelect';
import getOptions from '../../utils/getOptions';
import Button from '../../_ui/Button/Button';
import Table from '../../_ui/Table/Table';
import { journalColumns } from './JournalColumns';
import { useDispatch, useSelector } from 'react-redux';
import { journalModule, loadJournals } from './JournalDucks';
import useQueryFilter from '../../components/Filters/useQueryFilter';
import { groupModule, loadGroups } from '../Groups/GroupDucks';
import CreateJournal from './CreateJournal';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import { Typography } from '@material-ui/core';
import Access from '../../components/access/Access';
import paths from '../../_helpers/paths';
import { PERMISSIONS } from '../../_helpers/permissions';

export const journalFilterName = '_journal';

export default function Journal() {
  const { journals, count } = useSelector(state => state[journalModule]);
  const { groups } = useSelector(state => state[groupModule]);
  const { queryFilter } = useQueryFilter({ index: journalFilterName });
  const dispatch = useDispatch();
  const modal = useSimpleModal();

  useEffect(() => {
    dispatch(loadGroups());
  }, []);

  return (
    <div className="px4 py2" style={{ height: '100%' }}>
      <div className="mb2">
        <Typography variant="h6" children={'Журнал'} />
      </div>
      <div className="flex justify-between py1">
        <div style={{ width: 200 }} className="px1">
          <FilterSelect
            placeholder={'Группы'}
            name={'groupId'}
            filterName={journalFilterName}
            options={getOptions(groups)}
          />
        </div>
        {queryFilter && queryFilter.groupId && (
          <Access permissions={PERMISSIONS.JOURNAL_CREATE_AND_EDIT}>
            <Button
              text={'Добавить'}
              onClick={() => modal.open({ groupId: queryFilter.groupId })}
            />
          </Access>
        )}
      </div>
      <Table
        onClickRow={({ original }) => {
          modal.open({ ...original, isRead: true });
        }}
        queryFilterName={journalFilterName}
        columns={journalColumns(modal.open)}
        data={journals}
        loading={false}
        total={count}
        loadData={filter => dispatch(loadJournals(filter))}
      />
      {modal.isOpen && <CreateJournal {...modal} queryFilter={queryFilter} />}
    </div>
  );
}
