import React, { useEffect, useState } from 'react';
import Button from '../../_ui/Button/Button';
import Table from '../../_ui/Table/Table';
import { loadUsers, usersModule, clear } from '../UserManagePage/Users/UsersDucks';
import { useDispatch, useSelector } from 'react-redux';
import { userColumns } from './userColumns';
import Modal from '../../_ui/Modal/Modal';
import { createJournal, updateJournal } from './JournalDucks';
import { Api } from '../../_helpers/service';

export const createJournalFilterName = '_createJournal';
export default function CreateJournal({ data, close, queryFilter }) {
  const { users, usersCount } = useSelector(state => state[usersModule]);
  const [journal, setJournal] = useState([]);
  const dispatch = useDispatch();
  const { isRead } = data;

  useEffect(() => () => dispatch(clear()), []);

  useEffect(() => {
    if (data.date && users.length) {
      Api.post('api/zhurnals/data', data)
        .then(({ data }) =>
          setJournal([
            ...users
              .filter(
                ({ groupId }) =>
                  groupId === data.groupId || groupId === queryFilter.groupId
              )
              .map(item => {
                const user = data.find(({ userId }) => userId === item.id);
                return user
                  ? {
                      ...item,
                      zhurnalId: user.zhurnalId,
                      assessmentLesson: user.assessmentLesson,
                      isAttended: user.attended,
                      firstname: user.firstName,
                      lastname: user.lastName
                    }
                  : item;
              })
          ])
        )
        .catch(e => console.log(e));
    } else {
      setJournal(users);
    }
  }, [users]);

  const onChange = (id, params) => {
    const newJournal = journal.map(item => {
      return item.id === id ? { ...item, ...params } : item;
    });
    setJournal([...newJournal]);
  };

  const onSave = () => {
    const params = {
      groupId: data.groupId,
      data: journal.map(({ id, isAttended, assessmentLesson }) => ({
        userId: id,
        isAttended,
        assessmentLesson
      }))
    };
    data.date
      ? dispatch(updateJournal({ ...params, zhurnalId: data.zhurnalId }, close))
      : dispatch(createJournal(params, close));
  };

  return (
    <Modal maxWidth={'lg'} title={''} onClose={() => close()}>
      <div className="px4 py2">
        <div className="flex  justify-end py1">
          {!isRead && <Button text={'Сохранить'} onClick={onSave} />}
        </div>
        <Table
          queryFilterName={createJournalFilterName}
          columns={userColumns(onChange, isRead)}
          data={journal}
          loading={false}
          total={journal.length}
          loadData={filter =>
            dispatch(loadUsers({ filter, groupId: queryFilter.groupId || data.groupId }))
          }
        />
      </div>
    </Modal>
  );
}
