import React from 'react';
import Checkbox from '../../_ui/Checkbox/Checkbox';
import Input from '../../_ui/Input/Input';

export const userColumns = (onChange, isRead) => {
  console.log(isRead)
  const columns = [
    {
      Header: '№',
      accessor: 'rowNum',
      width: 80,
      Cell: ({ original: { rowNum } }) => (
        <span className="flex justify-center col-12">{rowNum}</span>
      ),
      filterable: false
    },
    {
      Header: 'ФИО',
      accessor: 'ФИО',
      Cell: ({ original: { firstname, lastname } }) => (
        <span className="flex justify-center col-12">{`${firstname} ${lastname}`}</span>
      )
    },
    {
      Header: 'Присутствует',
      accessor: 'isAttended',
      width: 200,
      Cell: ({ original: { isAttended, id } }) => (
        <div className="flex justify-center col-12 ">
          <Checkbox
            disabled={isRead}
            checked={isAttended}
            withoutForm
            onChange={({ target: { checked } }) => onChange(id, { isAttended: checked })}
          />
        </div>
      ),
      filterable: false
    },
    {
      Header: 'Оценка',
      accessor: 'assessmentLesson',
      width: 100,
      Cell: ({ original: { id, assessmentLesson, isAttended } }) => (
        <div className="flex justify-center col-12">
          <Input
            style={{ width: 50 }}
            type={'number'}
            disabled={!isAttended || isRead}
            value={assessmentLesson}
            withoutForm
            onChange={({ target: { value } }) =>
              onChange(id, { assessmentLesson: value })
            }
          />
        </div>
      ),
      filterable: false
    }
  ];

  return columns;
};
