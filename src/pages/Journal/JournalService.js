import { Api } from '../../_helpers/service';
import axios from 'axios';

export const JournalApi = {
  loadJournals: params => {
    let data = new FormData();
    const json = JSON.stringify(params);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('filter', blob);
    return axios.post('api/zhurnals', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  },
  createJournal: values => Api.post('api/save/zhurnal', values),
  updateJournal: values => Api.put('api/change/zhurnal', values)
};
