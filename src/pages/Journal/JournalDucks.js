import { createReducer } from '@reduxjs/toolkit';
import { JournalApi } from './JournalService';

/**
 * Constants
 */

export const journalModule = 'journal';
export const JOURNALS = `${journalModule}/JOURNALS`;
export const CREATE = `${journalModule}/CREATE`;
export const UPDATE = `${journalModule}/UPDATE`;
export const LOADING = `${journalModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  journals: [],
  count: 0,
  loading: false
};

export default createReducer(initialState, {
  [JOURNALS]: (state, { payload: { data, count } }) => {
    state.journals = data;
    state.count = count;
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    const isCreated = state.journals.some(
      ({ groupName }) => groupName === payload.groupName
    );
    console.log(isCreated);
    if (!isCreated) {
      state.journals = [...state.journals, payload];
      state.count = state.count + 1;
    }

    state.loading = false;
  },

  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadJournals = filter => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await JournalApi.loadJournals(filter);
    dispatch({ type: JOURNALS, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const createJournal = (values, close) => async dispatch => {
  try {
    let { data } = await JournalApi.createJournal(values);
    dispatch({ type: CREATE, payload: data });
    close && close();
  } catch (e) {
    console.error(e);
  }
};

export const updateJournal = (values, close) => async dispatch => {
  try {
    let { data } = await JournalApi.updateJournal(values);
    dispatch({ type: CREATE, payload: data });
    close && close();
  } catch (e) {
    console.error(e);
  }
};
