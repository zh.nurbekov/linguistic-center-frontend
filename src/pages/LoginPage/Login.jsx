import React from 'react';
import { useDispatch } from 'react-redux';
import Button from '../../_ui/Button/Button';
import Form from '../../_ui/Form/Form';
import Input from '../../_ui/Input/Input';
import { login } from './LoginDucks';
import { Wrapper } from './LoginStyles';

function LoginPage() {
  const dispatch = useDispatch();

  return (
    <Wrapper>
      <div className="login-form">
        <div className="title">{'Авторизация'}</div>
        <Form
          initialValues={{ username: '', password: '' }}
          onSubmit={data => login(data)(dispatch)}
          // validationSchema={SignInSchema}
        >
          <Input
            style={{marginBottom:5}}
            fullWidth name="username" placeholder={'Имя пользователя'} />
          <Input
            fullWidth
            name="password"
            placeholder={'Пароль'}
            type="password"
            showPassViewButton={true}
          />
          <Button
            fullWidth
            type="submit"
            style={{ marginTop: 5 }}
            text={'Войти'}
            // loading={loadingLogin}
          />
        </Form>
      </div>
    </Wrapper>
  );
}

export default LoginPage;
