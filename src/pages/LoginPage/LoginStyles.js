import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  height: 100%;
  width: 100%;

  .login-form {
    position: absolute;
    width: 100%;
    max-width: 300px;
    display: inline-table;
    margin: auto;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    text-align: center;
    transform: translateY(-50px);
  }

  .title {
    font-size: 21px;
    font-weight: 300;
    margin-bottom: 15px;
    margin-top: -10px;
  }

  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active {
    z-index: 999;
    transition: background-color 5000s ease-in-out 0s;
    -webkit-transition-delay: 9999999s;
    border-radius: 4px;
    padding: 0 !important;
    margin: 8px 14px;
  }
`;
