import { Api } from '../../_helpers/service';

export const LoginApi = {
  login: values =>
    Api.post('api/login', values, {
      auth: {
        username: values.username,
        password: values.password
      }
    }),
  logout: () => Api.get('/logout'),
  getRole: id => Api.get(`api/role/${id}`),

  refresh: refreshToken =>
    Api.post('/refresh', { refresh_token: refreshToken, fingerprint: '***' })
};
