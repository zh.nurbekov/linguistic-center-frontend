import { createReducer } from '@reduxjs/toolkit';
import { LoginApi } from './login.service';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/history';
import { localSecureStorage } from '../../_app/App';
import { LOADING, ROLES } from '../UserManagePage/Roles/RolesDucks';
import { NotificationManager } from 'react-notifications';

/**
 * Constants
 */

export const loginModule = 'login';
export const LOGIN = `${loginModule}/LOGIN`;
export const PERMISSIONS = `${loginModule}/PERMISSIONS`;
export const LOGOUT = `${loginModule}/LOGOUT`;

/**
 * Reducer
 */

const initialState = {
  user: null,
  permissions: []
};

export default createReducer(initialState, {
  [LOGIN]: (state, action) => ({
    user: action.user
  }),
  [PERMISSIONS]: (state, action) => ({
    permissions: action.permissions
  }),
  [LOGOUT]: state => {
    state.user = null;
  }
});

/**
 * Actions
 */

export const login = values => async dispatch => {
  try {
    const { data } = await LoginApi.login(values);
    localSecureStorage.setItem('user', data);
    history.push(paths.homePage);
  } catch (e) {
    NotificationManager.error('Введен неверный логин или пароль');
  }
};

export const logout = () => async dispatch => {
  // await LoginApi.logout();
  localSecureStorage.removeItem('accessToken');
  localSecureStorage.removeItem('refreshToken');
  localSecureStorage.removeItem('expiredAt');
  localSecureStorage.removeItem('user');
  localSecureStorage.removeItem('permissions');
  history.push(paths.login);
};

export const getRole = id => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await LoginApi.getRole(id);
    localSecureStorage.setItem('permissions', JSON.parse(data.access_list));
    dispatch({ type: PERMISSIONS, permissions: JSON.parse(data.access_list) });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};
