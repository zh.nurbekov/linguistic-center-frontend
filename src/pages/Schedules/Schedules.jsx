import React, { useEffect } from 'react';
import Calendar from '../../components/Calendar/Calendar';
import { Paper } from '@material-ui/core';
import CreateLesson from './CreateLesson';
import useSimpleModal from '../../components/_hooks/useSimpleModal';
import { useDispatch, useSelector } from 'react-redux';
import {
  deleteTemplates,
  loadTemplate,
  loadTemplates,
  schedulesModule
} from './SchedulesDucks';
import { localSecureStorage } from '../../_app/App';

export default function Schedules() {
  const { schedulers } = useSelector(state => state[schedulesModule]);
  const modal = useSimpleModal();
  const dispatch = useDispatch();
  const user = localSecureStorage.getItem('user');

  useEffect(() => {
    user.userType === 2 ? dispatch(loadTemplate(user.id)) : dispatch(loadTemplates());
  }, []);

  // console.log(moment('2021-04-22T10:50:00').format('hh:mm'));
  // console.log(moment('2021-04-22T10:50:00').format('mm'));

  const deleteTemplate = tamplate => {
    dispatch(deleteTemplates(tamplate));
  };

  return (
    <>
      <div className="p3">
        <div className="mb2">
          <span className="fs-18 bold" variant="h6" children={'Расписания'} />
        </div>
        <Paper>
          <Calendar
            data={schedulers}
            onCreateLesson={data => modal.open(data)}
            deleteTemplate={deleteTemplate}
          />
        </Paper>
      </div>
      {modal.isOpen && <CreateLesson {...modal} />}
    </>
  );
}

export const data = [
  {
    teacher: 7,
    groupLevel: 'childer inssas',
    auditoriumNumber: 2,
    startDateTime: '2021-04-21T10:00:00',
    endDateTime: '2021-04-21T12:00:00'
  },
  {
    teacher: 7,
    groupLevel: 'childer inssas',
    auditoriumNumber: 2,
    startDateTime: '2021-04-21T10:00:00',
    endDateTime: '2021-04-21T12:00:00'
  },
  {
    teacher: 7,
    groupLevel: 'childer inssas',
    auditoriumNumber: 2,
    startDateTime: '2021-04-21T10:00:00',
    endDateTime: '2021-04-21T12:00:00'
  },
  {
    teacher: 7,
    groupLevel: 'childer inssas',
    auditoriumNumber: 2,
    startDateTime: '2021-04-21T10:00:00',
    endDateTime: '2021-04-21T12:00:00'
  },
  {
    teacher: 7,
    groupLevel: 'childer inssas',
    auditoriumNumber: 2,
    startDateTime: '2021-04-21T10:00:00',
    endDateTime: '2021-04-21T12:00:00'
  }
];
