import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import { DialogActions, DialogContent, useTheme } from '@material-ui/core';
import Modal from '../../_ui/Modal/Modal';
import Form from '../../_ui/Form/Form';
import * as yup from 'yup';
import Input from '../../_ui/Input/Input';
import Select from '../../_ui/Select/Select';
import Row from '../../_ui/Row';
import {
  loadGroups,
  loadTeachers,
  saveTimetables,
  schedulesModule
} from './SchedulesDucks';
import DateTimePicker from '../../_ui/DatePicker/DateTimePicker';

const Validate = yup.object().shape({
  teacher: yup.number().required('Обязательное поле для заполнения'),
  groupLevel: yup.number().required('Обязательное поле для заполнения'),
  auditoriumNumber: yup.number().required('Обязательное поле для заполнения'),
  startDateTime: yup.string().required('Обязательное поле для заполнения'),
  endDateTime: yup.string().required('Обязательное поле для заполнения')
});

function CreateLesson({ data, dataIsEmpty, close }) {
  const { teachers, groups } = useSelector(state => state[schedulesModule]);
  const dispatch = useDispatch();
  const { palette } = useTheme();
  const formData = {
    ...data,
    teacher: dataIsEmpty ? null : data.teacher.id,
    groupLevel: dataIsEmpty ? null : data.groupLevel.id
  };
  useEffect(() => {
    dispatch(loadGroups());
    dispatch(loadTeachers());
  }, []);

  const onSubmit = values => {
    dataIsEmpty? dispatch(saveTimetables(values, close)):
    dispatch(saveTimetables(values, close, true));
  };

  console.log(formData,teachers);

  return (
    <div>
      <Modal maxWidth={'lg'} title={'Добавление записи'} onClose={() => close()}>
        <Form
          validate={Validate}
          initialValues={formData}
          onSubmit={onSubmit}
          render={() => (
            <>
              <DialogContent dividers className={'pt1'}>
                <div className="p2 ">
                  <Row label={'Консультант'}>
                    <Select
                      fullWidth
                      variant={'outlined'}
                      options={teachers}
                      name={'teacher'}
                    />
                  </Row>
                  <Row label={'Группа'}>
                    <Select
                      fullWidth
                      variant={'outlined'}
                      options={groups}
                      name={'groupLevel'}
                    />
                  </Row>
                  <Row label="Номер аудитории">
                    <Input type={'number'} fullWidth name={'auditoriumNumber'} />
                  </Row>
                  <Row label="Время начало занятий">
                    <DateTimePicker name={'startDateTime'} />
                  </Row>
                  <Row label={'Время окончаний занятий'}>
                    <DateTimePicker name={'endDateTime'} />
                  </Row>
                </div>
              </DialogContent>
              <DialogActions>
                <Button
                  style={{ color: 'white', background: palette.error.main }}
                  children="Отмена"
                  variant={'contained'}
                  onClick={close}
                />
                <Button
                  type="submit"
                  variant={'contained'}
                  children={'Создать'}
                  style={{ color: 'white', background: palette.success.main }}
                />
              </DialogActions>
            </>
          )}
        />
      </Modal>
    </div>
  );
}

export default CreateLesson;
