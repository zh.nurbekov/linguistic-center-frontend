import { createReducer } from '@reduxjs/toolkit';
import { SchedulesApi } from './SchedulesService';
import { NotificationManager } from 'react-notifications';

/**
 * Constants
 */

export const schedulesModule = 'schedules';
export const SCHEDULERS = `${schedulesModule}/SCHEDULERS`;
export const ADD_SCHEDULER = `${schedulesModule}/ADD_SCHEDULER`;
export const DELETE = `${schedulesModule}/DELETE`;
export const TEACHER = `${schedulesModule}/TEACHER`;
export const GROUPS = `${schedulesModule}/GROUPS`;
export const CREATE = `${schedulesModule}/CREATE`;
export const UPDATE = `${schedulesModule}/UPDATE`;
export const LOADING = `${schedulesModule}/LOADING `;

/**
 * Reducer
 */

const initialState = {
  schedulers: [],
  teachers: [],
  groups: [],
  loading: false
};

export default createReducer(initialState, {
  [SCHEDULERS]: (state, { payload }) => {
    state.schedulers = payload;
    state.loading = false;
  },
  [ADD_SCHEDULER]: (state, { payload }) => {
    state.schedulers = [...state.schedulers, payload];
    state.loading = false;
  },
  [DELETE]: (state, { payload }) => {
    state.schedulers = [...state.schedulers.filter(({ id }) => id !== payload.id)];
    state.loading = false;
  },
  [TEACHER]: (state, { payload: { data, count } }) => {
    state.teachers = [
      ...data.map(item => {
        const name = `${item.username} ${item.middlename}`;
        return { ...item, value: item.id, name };
      })
    ];
    state.loading = false;
  },
  [CREATE]: (state, { payload }) => {
    state.users.content = [payload, ...state.users.content];
    state.users.count = state.users.count + 1;
  },
  [UPDATE]: (state, { payload }) => {
    state.users.content = [
      ...state.users.content.map(item => (item.id === payload.id ? payload : item))
    ];
  },
  [GROUPS]: (state, { payload: data }) => {
    state.groups = [
      ...data.map(item => {
        return { ...item, value: item.id, name: item.name };
      })
    ];
  },
  [LOADING]: (state, { loading }) => {
    state.loading = loading;
  }
});

/**
 * Actions
 */

export const loadTeachers = () => async dispatch => {
  try {
    dispatch({ type: LOADING, loading: true });
    let { data } = await SchedulesApi.loadTeachers();
    console.log(data);
    dispatch({ type: TEACHER, payload: data });
  } catch (e) {
    dispatch({ type: LOADING, loading: false });
    console.error(e);
  }
};

export const loadTemplate = id => async dispatch => {
  try {
    let { data } = await SchedulesApi.loadTemplate(id);
    dispatch({ type: SCHEDULERS, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const loadTemplates = () => async dispatch => {
  try {
    let { data } = await SchedulesApi.loadTemplates();
    dispatch({ type: SCHEDULERS, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const deleteTemplates = tamplate => async dispatch => {
  try {
    let { data } = await SchedulesApi.deleteTemplates(tamplate.id);
    dispatch({ type: DELETE, payload: tamplate });
    NotificationManager.success('Запись успешна удалена');
  } catch (e) {
    console.error(e);
  }
};

export const loadGroups = () => async dispatch => {
  try {
    let { data } = await SchedulesApi.loadGroups();
    dispatch({ type: GROUPS, payload: data });
  } catch (e) {
    console.error(e);
  }
};

export const saveTimetables = (values, modalOnClose, isEdit) => async dispatch => {
  try {
    let { data } = isEdit
      ? await SchedulesApi.updateTimetables(values)
      : await SchedulesApi.saveTimetables(values);
    if (data.status === 'FAILED') {
      NotificationManager.error(data.message);
    } else {
      NotificationManager.success('Запись успешно создано');
      dispatch({ type: ADD_SCHEDULER, payload: values });
      modalOnClose && modalOnClose();
    }
  } catch (e) {
    console.error(e);
  }
};
