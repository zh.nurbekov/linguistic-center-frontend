import { Api } from '../../_helpers/service';
import axios from 'axios';

export const SchedulesApi = {
  loadGroups: () => Api.get('api/groups'),
  loadTemplate: id => Api.get(`api/list/timetables/${id}`),
  loadTemplates: () => Api.get(`api/list/timetables`),
  deleteTemplates: (timetablesId) => Api.delete(`api/delete/timetables/${timetablesId}`),
  updateTimetables: values => Api.put('api/update/timetables', values),
  saveTimetables: values => Api.post('api/create/timetables', values),
  loadTeachers: () => {
    const filter = { userType: 2 };
    let data = new FormData();
    const json = JSON.stringify(filter);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    data.append('filter', blob);
    return axios.post('api/search/users', data, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
