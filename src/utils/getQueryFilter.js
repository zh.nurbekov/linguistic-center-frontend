export default function getQueryFilter() {
  if (window.location.search !== '') {
    try {
      return JSON.parse(getUrlParams(window.location.search).filter);
    } catch (err) {
      console.log(err);
      return {};
    }
  }
}

function getUrlParams(search) {
  let hashes = search.slice(search.indexOf('?') + 1).split('&');
  return hashes.reduce((params, hash) => {
    let [key, val] = hash.split('=');
    return Object.assign(params, { [key]: decodeURIComponent(val) });
  }, {});
}
