import { NotificationManager } from 'react-notifications';

export default function({ status, message }, finish) {
  if (status === 'SUCCESS') {
    NotificationManager.success(message);
    finish && finish();
  }
  if (status === 'FAILED') {
    NotificationManager.error(message);
  }
}
