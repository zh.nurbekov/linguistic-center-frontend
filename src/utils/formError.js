import { isEmpty } from 'lodash'

export function errorMsg(name, { errors, touched, submitCount }) {
  let errorMsg = getElement(errors, name);
  if (submitCount || getTouched(touched, name) && errorMsg) {
    return errorMsg
  }
  return "";
}

export function isError(name, { errors, touched, submitCount }) {
  if (!isEmpty(errors) || !isEmpty(touched) ) {
  }
  return (submitCount || getTouched(touched, name)) && Boolean(getElement(errors, name))
}

const getElement = (object, name) => {
  return object[name]
};

const getTouched = (object, name) => {
  const names = name.split('.');

  if (names.length === 1) {
    return object[names[0]]
  }

  let element = undefined;
  for (let i = 0; i < names.length; i++) {
    if (i === 0) {
      if (object[names[i]]) {
        element = object[names[i]];
        continue;
      }
      break;
    }
    element = element[names[i]];
  }
  return element
};