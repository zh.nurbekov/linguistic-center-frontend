//первое число должны быть больше или равно
//второе число должно быть меньше или равно
//если два условия совпадают то это нужный цвет
export default function (colors, value, defaultColor) {
  let resultColor = defaultColor;
  for (let item of colors) {
    const { color, test } = item;
    const testA = test[0] === null || value >= test[0];
    const testB = test[1] === null || value <= test[1];
    if (testA && testB) {
      resultColor = color;
    }
  }
  return resultColor;
}