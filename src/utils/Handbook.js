/**
 * Получаем справочники и сохраняем в localSecureStorage
 */

import {HandbookApi} from "../_helpers/service";
import {localSecureStorage} from "../_app/App";

export async function loadHandbooks() {
  let { data: handbooks } = await HandbookApi.handbook();

  const eventTypes = handbooks.data.filter(i => i.title === 'event_type')[0].value.map(item => ({
    value: +item.id,
    name: item.title
  }));

  const eventSource = handbooks.data.filter(i => i.title === 'event_source')[0].value.map(item => ({
    value: +item.id,
    name: item.title
  }));

  const instanceStatuses = handbooks.data.filter(i => i.title === 'instance_status')[0].value.map(item => ({
    value: +item.id,
    name: item.title
  }));


  const handbook = {
    eventTypes: eventTypes,
    eventSource: eventSource,
    instanceStatuses: instanceStatuses,
  };
  localSecureStorage.setItem('handbook', handbook);

  return handbook;
}

/**
 * Получаем справочники из localSecureStorage и добавляем свойства для select
 */

export function handbooks() {
  let handbook = localSecureStorage.getItem('handbook');
  if (!handbook || Object.keys(handbook).length === 0) {
    handbook = loadHandbooks();
  }

  return localSecureStorage.getItem('handbook');
}
