/*
 * в двнной функций rangeAt не включительно , rangeTo - включительно
 * */

export default function(colors, total, defaultColor = 'transparent') {
  let resultColor = defaultColor;
  if (total !== undefined && total !== null && colors) {
    for (let item of colors) {
      const { color, rangeAt, rangeTo } = item;

      if (rangeAt < total && rangeTo >= total) {
        resultColor = color;
      }

      if (rangeAt === undefined && rangeTo) {
        total <= rangeTo && (resultColor = color);
      }

      if (rangeTo === undefined && rangeAt) {
        total > rangeAt && (resultColor = color);
      }

      if (rangeTo === rangeAt) {
        total === rangeAt && (resultColor = color);
      }
    }
  }
  return resultColor;
}
