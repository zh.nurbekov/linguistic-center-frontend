import swal from 'sweetalert';

export const changeStatus = (data, titleOn, titleOff, isActive, update) => {
  swal({
    icon: 'warning',
    dangerMode: true,
    buttons: {
      cancel: 'Отмена',
      confirm: 'Да'
    },
    text: isActive ? titleOn : titleOff
  }).then(confirm => confirm && update());
};

export const confirm = (title, update, cancelBtn = 'Отмена', okBtn = 'Да') => {
  swal({
    icon: 'warning',
    dangerMode: true,
    buttons: {
      cancel: cancelBtn,
      confirm: okBtn
    },
    text: title
  }).then(confirm => confirm && update());
};
