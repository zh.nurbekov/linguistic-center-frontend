import React from 'react';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import "moment/locale/kk";
import "moment/locale/ru";


function DatePickerProvider({  children }) {
  return (
      <MuiPickersUtilsProvider utils={MomentUtils} locale={'ru'}>
        {children}
      </MuiPickersUtilsProvider>
  );
}

export default DatePickerProvider;
