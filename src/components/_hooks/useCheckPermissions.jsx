import React from "react"
import {UseLocalSecureStorage} from "../../utils/UseLocalSecureStorage";

const useCheckPermissions = () => {
  const { getItem } = UseLocalSecureStorage()
  const user = getItem("user")

  const checkPermissions = (permissions) => {
    if (!user || !user.role || !permissions) {
      return false
    }

    if (user.role.is_admin) {
      return true
    }

    if (typeof permissions === 'object') {
      return !!(user.role.access_list && permissions.some(perm => {
        return user.role.access_list.some(access => access === perm)
      }))
    }

    return !!(user.role.access_list && user.role.access_list.some(item => item === permissions))
  }

  const noHasPermissionOrRight = (item) => {
    return item.permission === undefined || checkPermissions(item.permission)
  }

  return { checkPermissions, noHasPermissionOrRight }
}

export  default useCheckPermissions