import { useState } from 'react';

function useSimpleModal() {
  const [data, setData] = useState(null);
  return {
    data,
    isOpen: !!data,
    open: (data) => {
      setData(data)
    },
    dataIsEmpty: data && (typeof data === "object" ? Object.keys(data).length <= 0 : false),
    close: () => setData(null)
  };
}

export default useSimpleModal;