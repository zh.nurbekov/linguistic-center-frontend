import React, {useState} from 'react'

function useDialogModal() {
    const [data, setData] = useState(null);
    return {
        data,
        isOpen: !!data,
        open: setData,
        dataIsEmpty: data && Object.values(data).length <= 0,
        close: () => setData(null)
    };
}

export default useDialogModal;