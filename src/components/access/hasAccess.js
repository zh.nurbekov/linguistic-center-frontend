import React from 'react';
import { localSecureStorage } from '../../_app/App';

export default function(permissions, accessible) {
  const userPermission = localSecureStorage.getItem('permissions');
  console.log(permissions);

  let isAccess = false;
  if (accessible) {
    isAccess = true;
  } else if (userPermission) {
    if (permissions instanceof Array) {
      if (userPermission.some(item => permissions.includes(item))) isAccess = true;
    } else {
      if (userPermission.includes(permissions)) isAccess = true;
    }
  }

  return isAccess;
}
