import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { dialogModule } from './DialogModalDucks';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const DialogModal = () => {
  const { title, contentText, buttons, isOpen, icon, onClose } = useSelector(
    state => state[dialogModule]
  );

  const Buttons = ({ buttons }) => (
    <div className="flex justify-center fullWidth">
      {buttons
        ? buttons.map(item => (
            <Button
              {...item}
              key={item.title}
              children={item.title}
              onClick={item.onClick}
              color={item.color || 'primary'}
              style={item.style}
            />
          ))
        : []}
    </div>
  );
  return (
    <Dialog
      onClose={onClose}
      maxWidth={'xs'}
      open={isOpen}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      {onClose && (
        <div className="right-align">
          <IconButton aria-label="close" onClick={onClose}>
            <CloseIcon />
          </IconButton>
        </div>
      )}
      <DialogContent style={{ maxWidth: 390, textAlign: 'center', paddingTop: 50 }}>
        {icon ? icon : null}
        {title && (
          <DialogTitle style={{ padding: 16 }} id="alert-dialog-title" children={title} />
        )}
        <DialogContentText id="alert-dialog-description">{contentText}</DialogContentText>
      </DialogContent>
      <DialogActions style={{ padding: 20 }}>
        <Buttons buttons={buttons} />
      </DialogActions>
    </Dialog>
  );
};

export default DialogModal;
