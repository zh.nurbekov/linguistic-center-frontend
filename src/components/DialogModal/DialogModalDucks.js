import { createReducer } from '@reduxjs/toolkit';

export const dialogModule = 'dialogModule';
const ON_OPEN = `${dialogModule}/ON_OPEN`;
const ON_CLOSE = `${dialogModule}/ON_CLOSE`;

const initialState = {
  title: '',
  contentText: '',
  buttons: [],
  icon: null,
  isOpen: false
};

export default createReducer(initialState, {
  [ON_OPEN]: (state, action) => {
    state.title = action.payload.title;
    state.contentText = action.payload.contentText;
    state.buttons = action.payload.buttons || [];
    state.icon = action.payload.icon || null;
    state.isOpen = true;
    state.onClose = action.payload.onClose;
  },
  [ON_CLOSE]: () => {
    return initialState;
  }
});

export const openDialog = payload => async dispatch => {
  dispatch({
    type: ON_OPEN,
    payload
  });
};

export const closeDialog = () => async dispatch => {
  dispatch({
    type: ON_CLOSE
  });
};
