import { useEffect, useState } from 'react';
import React from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import useColumnFilter from './useColumnFilter';

function FilterInputField(props) {
  const { name, minLength, filterName = '', type = 'text', reset, ...restProps } = props;
  const filter = useColumnFilter(name, filterName);
  const [timeOutId, registerTimeOut] = useState(null);
  const [value, setValue] = useState('');
  const isText = type === 'text';

  useEffect(() => {
    const filterValue = filter.value || '';
    const newValue = minLength
      ? filterValue.length >= minLength
        ? filterValue
        : value
      : filterValue;
    setValue(newValue);
  }, [filter.value]);

  const clear = () => {
    filter.setFilter(null);
    setValue('');
  };

  useEffect(() => {
    setValue('');
  }, [reset]);

  const handleChange = value => {
    if (isText && minLength && value.length <= 2) {
      if (filter.value) {
        clearTimeout(timeOutId);
        filter.setFilter(null);
      }
      setValue(value);
    } else {
      if (parseInt(value) || isText) {
        clearTimeout(timeOutId);
        registerTimeOut(
          setTimeout(() => {
            filter.setFilter(value);
          }, 1000)
        );
        setValue(value);
      }
    }
  };

  const inputProps = {
    endAdornment: value.length > 0 && (
      <InputAdornment position="end">
        <IconButton onClick={clear} size="small">
          <CloseIcon fontSize="small" />
        </IconButton>
      </InputAdornment>
    )
  };

  return (
    <div style={{ width: '100%' }}>
      <TextField
        fullWidth
        variant="standard"
        value={value}
        InputProps={inputProps}
        onChange={e => handleChange(e.target.value)}
        inputProps={{ style: { paddingRight: 0, paddingLeft: 8 } }}
        {...restProps}
      />
      {minLength && value.length <= minLength - 1 && value.length > 0 && (
        <div className="center" style={{ color: 'red' }}>
          {'минимум 3 символа'}
        </div>
      )}
    </div>
  );
}

export default FilterInputField;
