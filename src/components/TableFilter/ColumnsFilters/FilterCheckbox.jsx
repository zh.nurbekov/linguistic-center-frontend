import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import useColumnFilter from "./useColumnFilter";

function FilterCheckbox({ name, label, queryIndex = '' }) {
  const { value, setFilter } = useColumnFilter(name, queryIndex);

  return (
    <FormControlLabel
      name={name}
      control={
        <Checkbox
          checked={value || false}
          color="primary"
          onChange={event => setFilter(event.target.checked)}
        />
      }
      label={label}
    />
  );
}

export default FilterCheckbox;
