import React, { useCallback } from 'react';
import { KeyboardDatePicker } from '@material-ui/pickers';
import CloseIcon from '@material-ui/icons/Close';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import useColumnFilter from "./useColumnFilter";

function FilterDatePicker({ name,  queryIndex = '' }) {
  const { value, setFilter } = useColumnFilter(name, queryIndex);
  const textFieldComponent = useCallback(props => {
    return (
      <TextField
        {...props}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              {props.value && (
                <IconButton className="show-on-hover" onClick={() => setFilter(null)}>
                  <CloseIcon fontSize="small" />
                </IconButton>
              )}
              {props.InputProps.endAdornment.props.children}
            </InputAdornment>
          )
        }}
      />
    );
    // eslint-disable-next-line
  }, []);

  return (
    <KeyboardDatePicker
      name={name}
      value={value}
      variant="inline"
      disableToolbar
      strictCompareDates
      format="DD.MM.YYYY"
      autoOk
      invalidDateMessage={ 'Недопустимый формат даты' }
      onChange={date =>
        setFilter(date && date.isValid() ? date.format('YYYY-MM-DDTHH:mm:ss') : null)
      }
      TextFieldComponent={textFieldComponent}
    />
  );
}

export default FilterDatePicker;
