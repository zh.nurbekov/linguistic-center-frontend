import React from 'react';
import { FormControl, IconButton, MenuItem, InputLabel } from '@material-ui/core';
import MuiSelect from '@material-ui/core/Select';
import CloseIcon from '@material-ui/icons/Close';
import useColumnFilter from './useColumnFilter';

function FilterSelect(props) {
  const { name, options, filterName = '', label, placeholder, ...restProps } = props;
  const { value, setFilter } = useColumnFilter(name, filterName);

  const clearFiled = value && {
    IconComponent: () => (
      <IconButton
        onClick={() => setFilter(null)}
        size="small"
        style={{ marginRight: 5 }}
        children={<CloseIcon fontSize="small" />}
      />
    )
  };

  return (
    <FormControl fullWidth margin="none" className="left-align">
      <InputLabel>{label}</InputLabel>
      <MuiSelect
        // variant={'outlined'}
        MenuProps={{ MenuListProps: { style: { maxWidth: 550, minWidth: 250 } } }}
        value={value || placeholder || ''}
        onChange={e => setFilter(e.target.value)}
        SelectDisplayProps={{ style: { paddingRight: 0, paddingLeft: 8 } }}
        {...clearFiled}
        {...restProps}
      >
        {placeholder && (
          <MenuItem value={placeholder} disabled>
            <span children={placeholder} style={{ color: '#BFBFBF' }} />
          </MenuItem>
        )}
        {options.map(
          item =>
            item && (
              <MenuItem key={item.value} value={item.value}>
                {item.name}
              </MenuItem>
            )
        )}
      </MuiSelect>
    </FormControl>
  );
}

export default FilterSelect;
