import React from 'react';
import useWidthStyle from './useWidthStyle';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import { Wrapper } from './FooterStyle';
import MailIcon from '@material-ui/icons/Mail';
import PhoneIcon from '@material-ui/icons/Phone';

function Footer() {
  return (
    <>
      <hr style={{ marginTop: 300 }} />
      <footer className="footer" style={{zIndex:20}}>
        <div
          className="px4 py3 flex justify-end"
          style={{ color: 'white', paddingLeft: 100, paddingRight: 100 }}
        >
          <div>
            <div className="flex items-center">
              {/*<PersonPinIcon fontSize={'large'} style={{ color: 'white' }} />*/}
              <div className="ml1">
                <div className="fs-14">ул. Глушко 2 , 123 кабинет</div>
                {/*<div className="fs-14"> 123 кабинет</div>*/}
              </div>
            </div>
            <div>
              <div className="flex items-center">
                {/*<PhoneIcon style={{ color: 'white' }} />*/}
                <div className="fs-13 ml1"> +7 (771) 524-66-89</div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
