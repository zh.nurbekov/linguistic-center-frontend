export default function(queryFilterName = '') {
  const filterName = `filter${queryFilterName}`;
  const params = new URLSearchParams(window.location.search);
  const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : {};
  return filter;
}
