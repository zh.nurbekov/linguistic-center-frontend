import { useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import checkFilter from './checkFilter';

const DEFAULT_FILTER = { limit: 10, offset: 0 };
export default function useQueryFilter({ index = '', defaultFilter }) {
  const filterName = `filter${index}`;
  const orderName = `order${index}`;
  const [queryFilter, setQueryFilter] = useState(null);
  const { pathname, search } = useLocation();
  const { replace } = useHistory();
  const params = new URLSearchParams(search);
  const filter = params.get(filterName)
    ? JSON.parse(params.get(filterName))
    : {...DEFAULT_FILTER,...defaultFilter};

  const reset = () => {
    params.set(filterName, JSON.stringify({...DEFAULT_FILTER,...defaultFilter}));
    replace(`${pathname}?${params.toString()}`);
  };

  const setFilter = param => {
    let requestFilter = checkFilter(filter, param);
    params.set(filterName, JSON.stringify(requestFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  useEffect(() => {
    let requestFilter = { ...defaultFilter, ...filter };
    if (JSON.stringify(requestFilter) !== JSON.stringify(queryFilter)) {
      setFilter(requestFilter);
      setQueryFilter({ ...requestFilter });
    }

  }, [search]);

  const clearFilter = () => () => {
    if (window.location.search) {
      if (pathname === window.location.pathname) {
        const params = new URLSearchParams(search);
        params.delete(filterName);
        params.delete(orderName);
        replace(`${pathname}?${params.toString()}`);
      }
    }
  };

  useEffect(() => clearFilter(), []);

  return { queryFilter, reset, index, setFilter };
}
