import { useLocation, useHistory } from 'react-router-dom';
import { useEffect, useState } from 'react';
import checkFilter from './checkFilter';

export default function usePushNewPageWithFilter(index = '') {
  const filterName = `filter${index}`;
  const { pathname, search } = useLocation();
  const { replace } = useHistory();
  const params = new URLSearchParams(search);
  const [queryFilter, setQueryFilter] = useState({});
  const filter = params.get(filterName) ? JSON.parse(params.get(filterName)) : {};

  const push = (param = {}) => {
    let requestFilter = checkFilter(filter, param);
    params.set(filterName, JSON.stringify(requestFilter));
    replace(`${pathname}?${params.toString()}`);
  };

  useEffect(() => {
    if (JSON.stringify(filter) !== JSON.stringify(queryFilter)) {
      setQueryFilter(filter);
    }
  }, [search]);

  return { push, queryFilter };
}
