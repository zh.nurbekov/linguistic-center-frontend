export default function(isDark) {
  const primary = {
    main: isDark ? '#e4e4e4' : '#20393c'
  };
  const secondary = { main: '#20393c', dark: '#36585c' };
  return {
    isDark: isDark,
    palette: {
      type: isDark ? 'dark' : 'light',
      primary: primary,
      secondary: secondary,
      background: {
        default: isDark ? '#191919' : '#f2f4f5'
      }
    },
    shape: { borderRadius: 8 },
    typography: {
      fontFamily:
        "'Jangle', -apple-system, BlinkMacSystemFont, Roboto, Helvetica Neue, sans-serif ",
      htmlFontSize: 16
    },
    props: {
      MuiButton: { disableFocusRipple: true },
      MuiButtonGroup: { disableElevation: true },
      MuiCard: { variant: 'outlined' },
      MuiAppBar: {
        position: 'static',
        color: 'secondary',
        elevation: 0
      },
      MuiOutlinedInput: {
        margin: 'dense'
      },
      MuiTextField: {
        variant: 'outlined',
        size: 'small'
      }
    },
    overrides: {
      MuiBottomNavigationAction: {
        root: {
          '&$selected': {
            color: secondary.dark
          }
        }
      },
      MuiMenuItem: {
        root: { fontSize: '15px' }
      },
      MuiPopover: {
        paper: {
          borderRadius: 0,
          border: '0.5px solid rgba(118, 118, 118, 0.28)',
          boxShadow: 'rgba(0, 0, 0, 0.15) 0px 10px 37px'
        }
      },
      MuiPaper: { rounded: { borderRadius: 2 } },
      MuiToolbar: {
        root: {
          justifyContent: 'center'
        }
      },
      MuiAppBar: {
        root: {
          backgroundColor: isDark ? '#262626' : '#ffffff'
        }
      },
      MuiFormControlLabel: {
        root: {
          marginRight: 0
        }
      },
      MuiCheckbox: {
        root: {
          padding: 0,
          marginRight: 7,
          marginLeft: 9
        }
      },
      MuiButton: {
        root: {
          padding: '6px 20px',
          textTransform: 'inherit',
          whiteSpace: 'nowrap'
        },
        outlined: {
          border: `1px solid rgba(${isDark ? '255, 255, 255,' : '0, 0, 0,'} 0.3)`,
          padding: '5px 20px'
        },
        outlinedSizeLarge: {
          paddingTop: 6,
          paddingBottom: 6
        },
        contained: {
          backgroundColor: isDark ? '#808080' : '#ffffff'
        },
        text: {
          padding: '5px 20px'
        },
        textSizeSmall: {
          padding: '4px 14px'
        }
      },
      MuiButtonBase: {
        root: {
          margin: null
        }
      },
      MuiOutlinedInput: {
        notchedOutline: {
          borderColor: `rgba(${isDark ? '255, 255, 255,' : '0, 0, 0,'} 0.3)`
        },
        inputMarginDense: {
          height: 'inherit',
          fontSize: '14px'
        }
      },
      MuiInputBase: {
        input: {
          color: isDark ? '#fff' : '#000'
        }
      },
      MuiTypography: {
        overline: {
          fontSize: '11px'
        }
      }
    }
  };
}
