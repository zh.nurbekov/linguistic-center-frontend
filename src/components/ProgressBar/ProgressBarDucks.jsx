/**
 * Constants
 */
import { createReducer } from '@reduxjs/toolkit';


export const progressModule = "progress";
export const SHOW = `${progressModule}/LOGIN`;
export const HIDE = `${progressModule}/LOADING`;


/**
 * Reducer
 */


const initialState = {
	show: false
};

export default createReducer(initialState, {
	[SHOW]: (state, action) => ({
		...state,
		show: true
	}),
	[HIDE]: (state, action) => ({
		...state,
		show: false
	})
});


/**
 * Actions
 */

export const showProgress = () => ({ type: SHOW });

export const hideProgress = () => ({type: HIDE});


