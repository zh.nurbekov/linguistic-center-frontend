import React from "react";
import { Div } from './ProgressBarStyle'
import { connect } from 'react-redux';

class ProgressBar extends React.Component {
  render() {
    return (
      <Div show={this.props.show}>
        <div className="circle-bg">
          <svg className="circular" viewBox="25 25 50 50">
            <circle
              className="path"
              cx="50"
              cy="50"
              r="20"
              fill="none"
              strokeWidth="2"
              strokeMiterlimit="10"
            />
          </svg>
        </div>
      </Div>
    );
  }
}

const stateProps = state => ({
  show: state.progress.show
});

export default connect(stateProps)(ProgressBar);
