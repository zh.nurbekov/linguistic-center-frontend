import React from 'react';
import { Typography, useTheme } from '@material-ui/core';

export default function Answers({ question }) {
  const { palette } = useTheme();
  console.log(question);
  return (
    <div className="mt2">
      <h3 children={question.name} />
      <div>
        <div>
          {question.image && question.image.indexOf('null') < 0 && (
            <img src={question.image} width={300} />
          )}
          <div className='ml3'>
            {question.answerValues.map((item, index) => {
              return question.questionId === item.id ? (
                <Typography
                  children={item.name}
                  style={{
                    color: question.answerCorrect ? palette.success.main : palette.error.main
                  }}
                />
              ) : (
                <Typography
                  children={item.name}
                  style={{
                    color: item.correct ? palette.success.main : palette.text.primary
                  }}
                />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
