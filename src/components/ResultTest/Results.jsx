import { useDispatch } from 'react-redux';
import React, { useEffect, useState } from 'react';
import { DialogContent, Paper, Typography, useTheme } from '@material-ui/core';
import { Api } from '../../_helpers/service';
import Answers from './Answers';

let isCorrectAnswerCount = 0;
export default function Results({ data: user, close }) {
  const [results, setResults] = useState([]);
  const { palette } = useTheme();
  const dispatch = useDispatch();

  useEffect(() => {
    Api.get(`api/test/result/${user.id}`)
      .then(({ data }) => {
        setResults(data);
        isCorrectAnswerCount = data.filter(({ answerCorrect }) => answerCorrect).length;
      })
      .catch(e => console.log(e));
  }, []);

  return (
    <div className='p3'>
      <div className="mb2">
        <Typography
          variant="h6"
          children={`Результат тестирование :  ${isCorrectAnswerCount} из ${results.length}`}
        />
      </div>
      <div className="p3">
        <div className="mb2">
          <Typography variant="h6" children={`${user.firstname} ${user.lastname}`} />
        </div>
        <div className="px4 py2">
          <DialogContent className={'pt1'}>
            {results.map((item, index) => (
              <div className="p2 mb2">
                <Answers question={item} />
              </div>
            ))}
          </DialogContent>
        </div>
      </div>
    </div>
  );
}
