import { history } from '../../../_helpers/history';
import paths from '../../../_helpers/paths';
import React from 'react';


export const MenuItem = [
  {title:'Главная', onClick:() => history.push(paths.homePage)},
  {title:'Услуги', onClick:() => history.push(paths.service)},
  {title:'Отзывы', onClick:() => history.push(paths.review)},
  {title:'Контактные данные', onClick:() => history.push(paths.contacts)},
  {title:'Достижения', onClick:() => history.push(paths.achievements)},
  {title:'Подача заявки', onClick:() => history.push(paths.filingApplication)},
]