import styled from 'styled-components';

export const StyledSearchFilter = styled.div`
  height: 55px;

  .filter {
    //position: fixed;
    display: flex;
    top: 64px;
    height: 55px;
    background: ${props => (props.theme.isDark ? '#f1f1f1' : '#f1f1f1')};
    width: 100%; //calc(100vw - 17px);
    z-index: 1;
    border-top: 1px solid ${props => props.theme.palette.divider};
    border-bottom: 1px solid ${props => props.theme.palette.divider};
    box-shadow: rgba(0, 0, 0, 0.1) 0 1px 12px;

    .sf-item {
      flex: 1;
      border-radius: 0 !important;
      height: 48px;
      font-size: 14px;
      white-space: inherit !important;

      > button {
        white-space: inherit !important;
        min-width: 30px !important;
        height: 48px;
        border-right: 1px solid ${({ theme }) => theme.palette.divider};
        border-radius: 0 !important;
        //padding-left: 5px;
        fieldset {
          display: none;
        }
      }

      :first-child > button {
        padding-left: 0;
      }

      :last-child > button {
        padding-right: 0;
        border: none;
      }
    }
  }
`;
