import React from 'react';
import { StyledSearchFilter } from './MenuStyle';
import { Badge, useTheme } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { MenuItem } from './MenuItem';
import { localSecureStorage } from '../../../_app/App';
import routers from '../../../_helpers/routers';
import { history } from '../../../_helpers/history';
import { useSelector } from 'react-redux';
import { loginModule } from '../../../pages/LoginPage/LoginDucks';
import MailIcon from '@material-ui/icons/Mail';
import paths from '../../../_helpers/paths';
import { messageModule } from '../../../pages/Message/MessageDucks';

function Menu({ widthStyle }) {
  const theme = useTheme();
  const { permissions } = useSelector(state => state[loginModule]);
  const { countTotalUnread } = useSelector(state => state[messageModule]);
  const user = localSecureStorage.getItem('user');

  console.log(user);
  return (
    <StyledSearchFilter theme={theme}>
      <div className="filter">
        <div style={widthStyle}>
          <div className="flex">
            {user &&
              permissions &&
              routers
                .filter(
                  rout => rout.sideBarItem && permissions.includes(rout.permissions)
                )
                .map(({ path, title, icon }) => (
                  <div className="flex items-center">
                    <Button
                      style={{ minWidth: 30, whiteSpace: 'none' }}
                      className="sf-item"
                      onClick={() => history.push(path)}
                    >
                      {title}
                      {path === paths.message && (
                        <Badge
                          badgeContent={countTotalUnread}
                          color="error"
                          className="ml1"
                        >
                          <MailIcon color={'primary'} />
                        </Badge>
                      )}
                    </Button>
                  </div>
                ))}
            {user && user.userType === 1 && (
              <Button
                style={{ minWidth: 30, whiteSpace: 'none' }}
                className="sf-item"
                onClick={() => history.push(paths.review)}
              >
                Отзывы
              </Button>
            )}
          </div>
          {!user && (
            <div className="flex">
              {MenuItem.map(({ title, onClick }) => {
                console.log(title);
                return <Button className="sf-item" children={title} onClick={onClick} />;
              })}
            </div>
          )}
        </div>
      </div>
    </StyledSearchFilter>
  );
}

export default Menu;
