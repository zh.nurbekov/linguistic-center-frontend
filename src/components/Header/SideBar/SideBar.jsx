import React from 'react';
import routers from '../../../_helpers/routers';
import Drawer from '@material-ui/core/Drawer';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import { Wrapper } from './SideBarStyle';
import Typography from '@material-ui/core/Typography';
import { Badge, useTheme } from '@material-ui/core';
import { Link } from 'react-router-dom';
import paths from '../../../_helpers/paths';
import MailIcon from '@material-ui/icons/Mail';
import { useSelector } from 'react-redux';
import { messageModule } from '../../../pages/Message/MessageDucks';


function SideBar({ showSideBar, setShowSideBar }) {
  const { palette } = useTheme();
  const { countTotalUnread } = useSelector(state => state[messageModule]);

  return (
    <Drawer open={showSideBar} onClose={() => setShowSideBar(false)}>
      <Wrapper color={palette.divider}>
        <List>
          {routers
            .filter(rout => rout.sideBarItem)
            .map(({ path, title, icon }) => (
              <Link
                to={path}
                key={path}
                style={{ textDecoration: 'none' }}
                onClick={() => setShowSideBar(false)}
              >
                <ListItem button key={path} divider variant="middle">
                  <div className="flex justify-between items-centers col-12">
                    <Typography color={'textPrimary'}>{title}</Typography>
                    {path === paths.message && (
                      <Badge badgeContent={countTotalUnread} color="error">
                        <MailIcon color={'primary'} />
                      </Badge>
                    )}
                  </div>
                </ListItem>
              </Link>
            ))}
        </List>
      </Wrapper>
    </Drawer>
  );
}

export default SideBar;
