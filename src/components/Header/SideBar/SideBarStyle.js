import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 250px;
  height: 100%;
  padding-top: 80px;
  background-color: ${props => props.color};

  .MuiListItem-gutters {
    padding-left: 20px !important;
    padding-right: 20px !important;
  }

  .MuiListItem-dense {
    padding-top: 10px;
    padding-bottom: 10px;
  }
`;
