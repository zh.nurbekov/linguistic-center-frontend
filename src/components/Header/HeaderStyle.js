import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Logo = styled(Link)`
  font-size: 31px;
  font-weight: bold;
`;

export const HeaderStyles = styled.div`
  height: 64px;
  background: #20393c;

  .menu-item {
    height: 64px;
    border-radius: 0;
  }
`;
