import React, { useState } from 'react';
import { HeaderStyles, Logo } from './HeaderStyle';
import paths from '../../_helpers/paths';
import { history } from '../../_helpers/history';
import { AppBar, Toolbar, useTheme, Button, Typography } from '@material-ui/core';
import { useLocation } from 'react-router-dom';
import Menu from './Menu/Menu';
import { localSecureStorage } from '../../_app/App';
import Input from '../../_ui/Input/Input';
import Form from '../../_ui/Form/Form';
import { login, logout } from '../../pages/LoginPage/LoginDucks';
import { useDispatch } from 'react-redux';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import IconButton from '../../_ui/Button/IconButton';
import moment from 'moment';
const headerStyle = {
  width: '100%',
  paddingLeft: 15,
  paddingRight: 15,
  margin: 'auto'
};

function Header() {
  const { palette } = useTheme();
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const user = localSecureStorage.getItem('user');

  const isPayDay = () => {
    const day = moment().date();
    return day < 5 || day > 24;
  };

  return (
    <>
      <HeaderStyles>
        <AppBar
          elevation={0}
          style={{
            backgroundColor: 'white',
            borderBottom:
              pathname !== paths.homePage ? '1px solid ' + palette.divider : 'none'
          }}
        >
          <Toolbar style={{ padding: 0, margin: 'auto', ...headerStyle, minHeight: 64 }}>
            <div className="flex items-center fullWidth">
              <Logo to={paths.homePage} className="ml3">
                <div style={{ display: 'grid' }}>
                  <h5 style={{ color: 'black', padding: 0, margin: 0 }}>Step Up</h5>
                  <Typography
                    color={'primary'}
                    variant={'caption'}
                    children={'Лингвистический центр'}
                  />
                </div>
              </Logo>
              <div className="flex items-center ml4">
                {user && isPayDay() && (
                  <Typography
                    style={{ color: 'red' }}
                    variant={'caption'}
                    children={'Внимание необходимо оплатить обучение'}
                  />
                )}
              </div>

              <div style={{ flexGrow: 1, textAlign: 'right' }}>
                {user ? (
                  <div className="flex items-center justify-end">
                    <Typography
                      color={'primary'}
                      children={`${user.firstname} ${user.lastname}`}
                    />
                    <div>
                      <IconButton
                        onClick={() => {
                          dispatch(logout());
                          history.push(paths.homePage);
                        }}
                        icon={<ExitToAppIcon color={'primary'} />}
                      />
                    </div>
                  </div>
                ) : (
                  <Form
                    onSubmit={values => login(values)(dispatch)}
                    render={({ username, password }) => {
                      return (
                        <>
                          <Input
                            name={'username'}
                            placeholder={'Логин'}
                            style={{ marginRight: 20 }}
                          />
                          <Input
                            showPassViewButton
                            type="password"
                            name={'password'}
                            placeholder={'Пароль'}
                          />
                          <Button
                            disabled={!(username && password)}
                            //  onClick={() => {
                            //    // document.execCommand('ClearAuthenticationCache');
                            // //   dispatch(logout());
                            //  }}
                            type="submit"
                            className="ml2"
                            variant="text"
                            children="Войти"
                          />
                        </>
                      );
                    }}
                  />
                )}
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </HeaderStyles>
      <Menu widthStyle={headerStyle} />
    </>
  );
}

export default Header;
