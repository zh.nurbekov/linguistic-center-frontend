import React, { useState } from 'react';
import { Table, TableContainer, TableRow, Typography } from '@material-ui/core';
import TableBody from './TableBody';
import TableHead from './TableHead';
import * as moment from 'moment';
import RightIcon from '@material-ui/icons/ChevronRight';
import LeftIcon from '@material-ui/icons/ChevronLeft';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Access from '../access/Access';
import { PERMISSIONS } from '../../_helpers/permissions';
import { Api } from '../../_helpers/service';
import { localSecureStorage } from '../../_app/App';

const Calendar = ({ data, onCreateLesson, deleteTemplate }) => {
  const [month, setMonth] = useState(moment());

  return (
    <>
      <div className="p3">
        <div className="flex justify-between items-center mb2">
          <div>
            <Access permissions={PERMISSIONS.SCHEDULER_CREATE_AND_EDIT}>
              <Button
                color={'primary'}
                variant={'contained'}
                onClick={() => onCreateLesson({})}
              >
                <AddIcon />
                Добавить запись
              </Button>
            </Access>
            <Button
              style={{ marginLeft: 20 }}
              color={'primary'}
              variant={'contained'}
              onClick={() => {
                const user = localSecureStorage.getItem('user');
                Api.get(
                  user.userType === 2
                    ? `api/timetables/export/teacher/${user.id}`
                    : 'api/timetables/export'
                )
                  .then(data => {
                    // console.log(data);
                    // let blob = new Blob([data], {
                    //   type:
                    //     'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    // });
                    // saveAs(blob, 'excel.xml');
                  })
                  .catch(e => console.log(e));
              }}
              children={'Экспортировать в Excel'}
            />
          </div>
          <div className="flex items-center">
            <Typography children={month.format('MMMM').toLocaleUpperCase()} />
            <Button
              onClick={() => setMonth(moment(month.subtract(1, 'months')))}
              style={{ marginLeft: 10 }}
            >
              <LeftIcon />
            </Button>
            <Button onClick={() => setMonth(moment(month.add(1, 'months')))}>
              <RightIcon />
            </Button>
          </div>
        </div>

        <TableContainer style={{ border: '1px solid rgba(224, 224, 224, 1)' }}>
          <Table aria-label="caption table">
            <TableHead />
            <TableBody
              month={month}
              data={data}
              onCreateLesson={onCreateLesson}
              deleteTemplate={deleteTemplate}
            />
          </Table>
        </TableContainer>
      </div>
    </>
  );
};

export default Calendar;
