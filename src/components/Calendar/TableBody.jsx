import {
  TableBody as MuiTableBody,
  TableCell,
  TableRow,
  Typography
} from '@material-ui/core';
import React from 'react';
import { useTableBodyStyles } from './CalendarStyles';
import getCalendarDate from './getCalendarDate';
import moment from 'moment';
import { Button, Popconfirm } from 'antd';
import Access from '../access/Access';
import { PERMISSIONS } from '../../_helpers/permissions';

/***
 * @Props
 * align - left,center,right - расположение элементов хейдера
 ***/

const TableBody = ({ month, data, onCreateLesson, deleteTemplate }) => {
  const classes = useTableBodyStyles();

  return (
    <MuiTableBody>
      {getCalendarDate(month).map((week, index) => (
        <TableRow key={index}>
          {week.map(({ day }) => (
            <TableCell className={classes.cell} key={day} component="th" scope="row">
              <div style={{ height: '100%' }}>
                <Typography children={day} />
                {data
                  .filter(
                    ({ startDateTime }) =>
                      moment(startDateTime).format('M') === month.format('M')
                  )
                  .map(item => {
                    const startTime = moment(item.startDateTime).format('HH:mm');
                    const endTime = moment(item.endDateTime).format('HH:mm');
                    return moment(item.startDateTime).format('D') === String(day) ? (
                      <>
                        <div
                          style={{
                            background: 'cadetblue',
                            borderRadius: 8,
                            color: 'white'
                          }}
                          className="p1 mb1"
                        >
                          <div className=" flex">
                            <span className="fs-12 bold mr1">Время :</span>
                            <span className="fs-13">{`${startTime} - ${endTime}`}</span>
                          </div>
                          <div className="flex">
                            <span className="fs-12 bold mr1">Группа :</span>
                            <span className="fs-13">{item.groupLevel.name}</span>
                          </div>

                          <div className="flex">
                            <span className="fs-12 bold mr1">Кабинет :</span>
                            <span className="fs-13">{item.auditoriumNumber}</span>
                          </div>

                          {item.teacher && (
                            <div
                              style={{
                                background: '#476fac',
                                color: '#fff',
                                padding: 5,
                                borderRadius: 8
                              }}
                            >
                              <span className="fs-12 bold mr1">Консультант :</span>
                              <span className="fs-13">{`${item.teacher.firstname} ${item.teacher.middlename}`}</span>
                            </div>
                          )}
                          <Access permissions={PERMISSIONS.SCHEDULER_CREATE_AND_EDIT}>
                            <div className="flex items-center justify-center">
                              <Button
                                onClick={() => onCreateLesson(item)}
                                type={'link'}
                                children={'Редактировать'}
                                style={{
                                  color: 'white'
                                }}
                              />
                            </div>
                          </Access>
                          <Access permissions={PERMISSIONS.SCHEDULER_CREATE_AND_EDIT}>
                            <Popconfirm
                              placement="top"
                              title={'Вы хотите удалить запись ?'}
                              onConfirm={() => deleteTemplate(item)}
                              okText="Удалить"
                              cancelText="Нет"
                            >
                              <div className="flex items-center justify-center">
                                <Button
                                  type={'link'}
                                  children={'Удалить'}
                                  style={{
                                    color: 'white'
                                  }}
                                />
                              </div>
                            </Popconfirm>
                          </Access>
                        </div>
                      </>
                    ) : null;
                  })}
              </div>
            </TableCell>
          ))}
        </TableRow>
      ))}
    </MuiTableBody>
  );
};

export default TableBody;
