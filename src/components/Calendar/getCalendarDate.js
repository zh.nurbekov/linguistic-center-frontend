export default function(month) {
  const startDayInWeek = getStart(month);
  const endDayInWeek = getEnd(month);
  const currentMonth = [];
  let monthDay = 0;
  for (let w = 1; w <= 5; w++) {
    const weeks = [];
    for (let d = 1; d <= 7; d++) {
      let day = (w - 1) * 7 + d;
      if (startDayInWeek <= day) {
        monthDay++;
        day++;
        if (monthDay <= endDayInWeek) {
          weeks.push({ day: monthDay });
        } else {
          weeks.push({});
        }
      } else {
        weeks.push({});
      }
    }
    currentMonth.push(weeks);
  }
  return currentMonth;
}

const getStart = month => month.startOf('month').isoWeekday();
const getEnd = month => month.endOf('month').date();