import {
  TableCell,
  TableHead as MuiTableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import React from 'react';
import { HeaderStyles, useStyles } from './CalendarStyles';

export const weeksDays = [
  { value: 'Monday', name: 'Пн' },
  { value: 'Tuesday', name: 'Вт' },
  { value: 'Wednesday', name: 'Ср' },
  { value: 'Thursday', name: 'Чт' },
  { value: 'Friday', name: 'Пт' },
  { value: 'Saturday', name: 'Сб' },
  { value: 'Sunday', name: 'Вс' }
];

const TableHead = () => {
  const classes = useStyles();

  return (
    <MuiTableHead>
      <TableRow>
        {weeksDays.map(({ value, name }, index) => {
          return (
            <TableCell
              onClick={() => console.log('onClick')}
              className={classes.headerCell}
              key={value}
            >
              <HeaderStyles index={index}>
                <Typography
                  variant={'subtitle2'}
                  children={name}
                  className="justify-end"
                />
              </HeaderStyles>
            </TableCell>
          );
        })}
      </TableRow>
    </MuiTableHead>
  );
};

export default TableHead;
