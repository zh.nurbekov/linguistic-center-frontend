import { makeStyles } from '@material-ui/core';
import styled from 'styled-components';

export const useStyles = makeStyles(theme => ({
  headerCell: {
    padding: 5,
    height: 48,
    cursor: 'pointer',
    border: '1px solid rgba(224, 224, 224, 1)'
  }
}));

export const useTableBodyStyles = makeStyles(theme => ({
  cell: {
    padding: '5px 10px 5px 10px',
    height: '100px !important ',
    marginRight: 5,
    border: '1px solid rgba(224, 224, 224, 1)',
    minHeight:'100px !important',
    width:200
  }
}));

export const HeaderStyles = styled.div`
  display: flex;
  align-items: center;
`;
